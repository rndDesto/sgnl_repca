module.exports = {
    'primary': {
      "0": "#FFF5F6",
      "100": "#FFEBEE",
      "200": "#FFCCD3",
      "300": "#FF99A8",
      "400": "#FF99A8",
      "500": "#FF3351",
      "600": "#FF0025",
      "700": "#E50021",
      "800": "#C2001C",
      "900": "#940015",
    },
    'secondary': {
      '0': '#FCFDFD',
      '100': '#F9F9FA',
      '200': '#F1F1F4',
      '300': '#DFE2E7',
      '400': '#B3BAC6',
      '500': '#8C99AC',
      '600': '#334867',
      '700': '#001A41',
      '800': '#000E2E',
      '900': '#00071F',
    },
    'valid': {
      '0': '#F2FDF4',
      '100': '#EDFCF0',
      '200': '#9AEEAB',
      '300': '#0FD277',
      '400': '#00B86B',
      '500': '#008E53',
      '600': '#007545',
      '700': '#005C36',
      '800': '#004227',
      '900': '#00331E',
    },
    'info': {
      '0': '#F7FCFF',
      '100': '#E9F6FF',
      '200': '#94CFF6',
      '300': '#5CA8E6',
      '400': '#3381CE',
      '500': '#0050AE',
      '600': '#003D95',
      '700': '#002D7D',
      '800': '#002064',
      '900': '#001653',
    },
    'warning': {
      '0': '#FEFBF1',
      '100': '#FEF3D4',
      '200': '#FEE5AA',
      '300': '#FED27F',
      '400': '#FDC05F',
      '500': '#FDA22B',
      '600': '#D9801F',
      '700': '#B66215',
      '800': '#92470D',
      '900': '#793408',
    },
    'error': {
      '0': '#FEEFEB',
      '100': '#FDDDD4',
      '200': '#FBB5A9',
      '300': '#F4837D',
      '400': '#E95B61',
      '500': '#DB2941',
      '600': '#BC1D42',
      '700': '#9D1440',
      '800': '#7F0D3C',
      '900': '#690739',
    }

};