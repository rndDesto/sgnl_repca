import { createComponent } from '@lit-labs/react';
import { css, html, LitElement, unsafeCSS } from 'lit';
import { customElement, property, state } from 'lit/decorators.js';
import React from 'react';
import globalStyles from '../../global.css?inline';
import styles from './textarea.scss?inline';
import { classMap } from 'lit-html/directives/class-map.js';
import { when } from 'lit/directives/when.js';
import { DeleteCircleIcon, InfoIcon } from '../../assets';
import { ifDefined } from 'lit/directives/if-defined.js';

@customElement('signal-textarea')
export class SignalTextArea extends LitElement {
    static styles = [
        unsafeCSS(globalStyles),
        css`${unsafeCSS(styles)}`
    ]

    /**
    * Text area label.
    */
    @property({ type: String })
    label?= 'Label'
    /**
    * Flag for error state.
    */
    @property({ type: Boolean })
    isError?= false
    /**
    * Error text.
    */
    @property({ type: String })
    errorText?= 'Error text'
    /**
    * Flag for valid state.
    */
    @property({ type: Boolean })
    isValid?= false
    /**
    * Valid text.
    */
    @property({ type: String })
    validText?= 'Valid text'
    /**
    * Helper text.
    */
    @property({ type: String })
    helperText?= 'Helper text'
    /**
    * Disabled text area.
    */
    @property({ type: Boolean })
    disabled?= false
    /**
    * Flag for show info state.
    */
    @property({ type: Boolean })
    showInfo?= false
    /**
    * Flag for mandatory state.
    */
    @property({ type: Boolean })
    isMandatory?= false
    /**
    * Placeholder text area.
    */
    @property({ type: String })
    placeholder?= 'Placeholder'
    /**
    * Text area without label.
    */
    @property({ type: Boolean })
    withoutLabel?= false
    /**
    * Value text area.
    */
    @property({ type: String })
    value?= ''
    /**
    * Max length value text area.
    */
    @property({ type: Number })
    maxLength?= 200
    /**
    * Min length value text field.
    */
    @property({ type: Number })
    minLength?= 0
    /**
    * Text area class.
    */
    @property({ type: String })
    class?= '';
    /**
    * Text area rows.
    */
    @property({ type: Number })
    rows?= 4;

    @state()
    counter = 0;


    private clearText() {
        let textareaId = this.shadowRoot?.getElementById('textarea') as HTMLInputElement;
        textareaId.value = '';
        this.value = '';
        this.counter = 0;
    }

    private onClickIconInfo() {
        const event = new CustomEvent('onClickIconInfo', { bubbles: true, composed: true });
        this.dispatchEvent(event);
    }

    private onChange(event: any) {
        let textareaId = this.shadowRoot?.getElementById('textarea') as HTMLInputElement;
        this.value = event.target.value;
        this.counter = textareaId.value.length;
        let customEvent = new CustomEvent('onChange', { bubbles: true, composed: true, detail: { value: this.value } });
        this.dispatchEvent(customEvent);
    }

    private onError() {
        let inputId = this.shadowRoot?.getElementById('input') as HTMLInputElement;
        this.value = inputId.value;
        let customEvent = new CustomEvent('onError', { bubbles: true, composed: true, detail: { value: this.value } });
        this.dispatchEvent(customEvent);
    }

    private onSubmit() {
        let inputId = this.shadowRoot?.getElementById('input') as HTMLInputElement;
        this.value = inputId.value;
        let customEvent = new CustomEvent('onSubmit', { bubbles: true, composed: true, detail: { value: this.value } });
        this.dispatchEvent(customEvent);
    }

    render() {
        const inputContainerStyles = classMap({
            [this.class!]: this.class!,
            'input-container body02-regular': true,

        })
        const inputStyles = classMap({
            'input-style body01-regular mb-[-4px]': true,
            'input-error': this.isError === true && this.isValid === false,
            'input-success': this.isValid === true && this.isError === false,
        });

        return html`
        <div class=${inputContainerStyles}>
            ${when(!this.withoutLabel, () => html`
                <div class="flex flex-row items-center">
                <p class="text-primary">
                    ${this.label}
                    ${when(this.isMandatory, () => html`<span>*</span>`)}
                </p>
                ${when(this.showInfo, () => html`
                    <button class="w-5 h-5 ml-1" @click=${this.onClickIconInfo}>
                        <img src=${InfoIcon} alt="Info Icon" />
                    </button>
                `)}
                </div>`)}
            <div class="relative">
                <textarea id="textarea" @keyup=${this.onChange} class=${inputStyles} placeholder=${ifDefined(this.placeholder)} ?disabled=${this.disabled} value=${ifDefined(this.value)} maxlength=${ifDefined(this.maxLength)} minlength=${ifDefined(this.minLength)} @error=${this.onError} @submit=${this.onSubmit} rows=${ifDefined(this.rows)}>${this.value}</textarea>
                ${when(this.counter > 0 && !this.disabled, () => html`<button class="reset-button" @click=${this.clearText}>
                <img src=${DeleteCircleIcon} alt="delete" />
                </button>`)}
            </div>
            <div class="input-description-container">
                ${when(this.helperText, () => html`<p class="body02-regular">${this.helperText}</p>`)}
                ${when(!this.isError && !this.isValid, () => html`<p class="text-secondary">${this.counter}/${this.maxLength}</p>`)}
            </div>
            <div>
                ${when(this.isError && !this.isValid, () => html`<p class="error-text">${this.errorText}</p>`)}
                ${when(this.isValid && !this.isError, () => html`<p class="valid-text">${this.validText}</p>`)}
            </div>
        </div>
        `
    }
}

declare global {
    interface HTMLElementTagNameMap {
        'signal-textarea': SignalTextArea
    }
}

export const SignalTextAreaReact = createComponent({
    tagName: 'signal-textarea',
    elementClass: SignalTextArea,
    react: React,
    events: {
        onChange: 'onChange',
        onClickIconInfo: 'onClickIconInfo',
        onError: 'onError',
        onSubmit: 'onSubmit'
    },
});