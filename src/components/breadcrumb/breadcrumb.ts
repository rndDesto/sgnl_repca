import { createComponent } from '@lit-labs/react';
import { css, html, LitElement, unsafeCSS } from 'lit';
import { customElement, property } from 'lit/decorators.js';
import React from 'react';
import globalStyles from '../../global.css?inline';
import styles from './breadcrumb.scss?inline';
import { when } from 'lit/directives/when.js';

@customElement('signal-breadcrumb')
export class SignalBreadcrumb extends LitElement {
    static styles = [
        unsafeCSS(globalStyles),
        css`${unsafeCSS(styles)}`
    ]

    constructor() {
        super();
        this.timeout = 0;
        this.active = false;
    }

    /**
    * Crumb array.
    */
    @property({ type: Array })
    crumbs?= [];
    /**
    * Button class.
    */
    @property({ type: String })
    class?= '';

    @property({ type: Number })
    timeout? = 0;

    @property({ type: Boolean})
    active? = false;

    private onClick() {
        const event = new CustomEvent('onClick', { bubbles: true, composed: true });
        this.dispatchEvent(event);
    }

    private isLast(index: number) {
        return (this.crumbs && index == this.crumbs.length - 1)
    }

    private showTip() {
        clearInterval(this.timeout);
        this.timeout = window.setTimeout(() => {
            this.active = true;
        }, 400);
    };

    private hideTip() {
        clearInterval(this.timeout);
        this.timeout = window.setTimeout(() => {
            this.active = false;
        }, 400);
    };

    render() {
        return html`
        <ul class="flex flex-row items-center text-left relative ${this.class}">
        ${when(this.crumbs, () => this.crumbs!.length < 5 ? this.crumbs?.map((item, index) => {
            const activeStyle = this.isLast(index) ? 'breadcrumb-active-menu title-bold' : '';
            return html`
                <li @click=${this.onClick} class=${activeStyle}>
                            ${this.isLast(index) ? html`<p class="text-primary title-bold" key={index}>
                            ${item['title']}</p>` : html`<div key={index} class="flex flex-row items-center text-secondary title-regular">
                            <a href=${item['route']}}>${item['title']}</a>
                            <div class="w-[8px]"></div>
                            <p>/</p>
                            <div class="w-[8px]"></div>
                    </div>`}
                </li>
                `
        }): 
        html`<div class="inline-block relative">
                <li class="flex flex-row items-center text-secondary title-regular text-left">
                    <a href=${this.crumbs![0]['route']}>${this.crumbs![0]['title']}</a>
                    <div class="w-[8px]"></div>
                    <p>/</p>
                    <div class="w-[8px]"></div>
                    <button @click=${this.showTip} @mouseenter="${this.showTip}" @mouseleave="${this.hideTip}">...</button>
                    <div class="w-[8px]"></div>
                    <p>/</p>
                    <div class="w-[8px]"></div>
                    ${this.crumbs?.slice(-2).map((items, index) =>
                        index === 1 ? html`<p class="text-primary title-bold text-left" key={index}>
                                ${items['title']}</p>`
                         : 
                          html  `<li key={index} class="flex flex-row items-center text-secondary title-regular">
                                <a href=${items['route']}>${items['title']}</a>
                                <div class="w-[8px]"></div>
                                <p>/</p>
                                <div class="w-[8px]"></div>
                            </li>`
                        )
                    }
                </li>
                ${when(this.active, () => 
                    html`
                    <div class='tooltip-tip bottom' @mouseenter="${this.showTip}" @mouseleave="${this.hideTip}">
                        ${this.crumbs?.slice(1, this.crumbs?.length - 2).map((items: any, index: any) => 
                            html `
                                <a href=${items['route']}
                                    class=${`py-[8px] text-left body01-regular text-primary min-w-[136px] ${
                                        index !== this.crumbs!.length - 4 ? 'mb-[16px]' : ''
                                        }`}
                                        key={index}
                                >
                                    ${items['title']}
                                </a>
                            `
                            )}
                        </div>
                    </div>`
                    )}
                `
        )}
        </ul>
        `
    }
}

declare global {
    interface HTMLElementTagNameMap {
        'signal-breadcrumb': SignalBreadcrumb
    }
}

export const SignalBreadcrumbReact = createComponent({
    tagName: 'signal-breadcrumb',
    elementClass: SignalBreadcrumb,
    react: React,
    events: {
        onClick: 'onClick',
    },
});