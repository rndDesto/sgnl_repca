import { createComponent } from '@lit-labs/react';
import { css, html, LitElement, unsafeCSS } from 'lit';
import { customElement, property } from 'lit/decorators.js';
import React from 'react';
import globalStyles from '../../global.css?inline';
import styles from './radio.scss?inline';
import { ifDefined } from 'lit/directives/if-defined.js';
import { classMap } from 'lit-html/directives/class-map.js';

@customElement('signal-radio')
export class SignalRadio extends LitElement {
    static styles = [
        unsafeCSS(globalStyles),
        css`${unsafeCSS(styles)}`
    ]

    /**
    * Checked radio.
    */
    @property({ type: Boolean })
    selected?= false;
    /**
    * Disabled radio.
    */
    @property({ type: Boolean })
    disabled?= false;
    /**
    * Radio class.
    */
    @property({ type: String })
    class?= '';
    /**
    * Radio label position.
    */
    @property({ reflect: true })
    labelPosition: 'left' | 'right' = 'right';
    /**
    * Radio value.
    */
    @property({ type: String })
    value?= '';

    private onChange() {
        let customEvent = new CustomEvent('onChange', { bubbles: true, composed: false, detail: { value: this.value } });
        this.dispatchEvent(customEvent);
    }

    render() {
        const radioStyle = classMap({
            [this.class!]: this.class!,
            'container-radio': true,
            'body01-regular': this.selected === false,
            'body01-bold': this.selected === true,
            'radio-left': this.labelPosition == 'left',
            'radio-right': this.labelPosition == 'right'
        });

        return html`
            <div class=${radioStyle}>
                <label>
                    <input id="radio" type="radio" value=${ifDefined(this.value)} ?checked=${this.selected} ?disabled=${this.disabled} @click=${this.onChange}/>
                    <slot></slot>
                </label>
            </div>
        `
    }
}

declare global {
    interface HTMLElementTagNameMap {
        'signal-radio': SignalRadio
    }
}

export const SignalRadioReact = createComponent({
    tagName: 'signal-radio',
    elementClass: SignalRadio,
    react: React,
    events: {
        onChange: 'onChange',
    },
});