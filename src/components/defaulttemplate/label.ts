import { css, html, LitElement, unsafeCSS } from 'lit';
import { classMap } from 'lit-html/directives/class-map.js';
import { customElement, property } from 'lit/decorators.js';
import globalStyles from '../../global.css?inline';
import styles from './button.scss?inline';

@customElement('signal-label')
export class SignalLabel extends LitElement {

    static styles = [
        unsafeCSS(globalStyles),
        css`${unsafeCSS(styles)}`
    ]
    
    @property({ type: String })
    partName?='';

    @property({ type: String })
    idName?='';

    @property({ type: String })
    testId?='';


    render() {
        const labelStyle = classMap({
            'body01-bold relative z-[1]': true,
        });

        return html`
        <div 
            data-testid=${this.testId === '' ?'tsel-labelid': this.testId}
            part=${this.partName === '' ?'tselpartlabel': this.partName}
            id=${this.idName === '' ?'tselidlabel': this.idName} 
            class=${labelStyle}
            >
                <slot></slot>
        </div>
        `
    }
}

declare global {
    interface HTMLElementTagNameMap {
        'signal-label': SignalLabel
    }
}