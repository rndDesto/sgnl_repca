import React from "react";
import { SignalLabel } from "./label";
import { createComponent } from "@lit-labs/react";

const SignalLabelReact = createComponent({
    tagName: 'signal-button',
    elementClass: SignalLabel,
    react: React,
    events: {
        onClick: 'onClick',
    },
});

export default SignalLabelReact