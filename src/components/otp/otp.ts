import { createComponent } from '@lit-labs/react';
import { css, html, LitElement, unsafeCSS } from 'lit';
import { classMap } from 'lit-html/directives/class-map.js';
import { customElement, property, state } from 'lit/decorators.js';
import React from 'react';
import globalStyles from '../../global.css?inline';
import styles from './otp.scss?inline';

@customElement('signal-otp')
export class SignalOTP extends LitElement {
    static styles = [
        unsafeCSS(globalStyles),
        css`${unsafeCSS(styles)}`
    ]

    /**
    * Digit OTP.
    */
    @property({ type: Number })
    digit: 4 | 5 | 6 = 6;
    /**
    * Error OTP.
    */
    @property({ type: Boolean })
    isError?= false;
    /**
    * Disabled OTP.
    */
    @property({ type: Boolean })
    disabled?= false;
    /**
    * OTP class.
    */
    @property({ type: String })
    class?= '';

    @state()
    arrDigit = [1, 2, 3 ,4 ,5, 6];

    @state()
    value = ''

    protected shouldUpdate(changedProperties: Map<any, any>) {
        changedProperties.forEach(() => {
          this.arrDigit = []
          if (changedProperties.has('digit')) {
            for (let i = 1; i <= this.digit; i ++) {
                this.arrDigit.push(i)
              }
          }
        });
        return changedProperties.has('digit');
    }

    private getCodeBoxElement(index: number) {
        return this.shadowRoot?.getElementById('codeBox' + index) as HTMLInputElement;
    }

    private onKeyUpEvent(index: any, event: any) {
        const eventCode = event.which || event.keyCode;
        if (this.getCodeBoxElement(index).value.length == 1) {
            if (index != this.digit) {
                this.getCodeBoxElement(index + 1).focus();
            } else {
                this.getCodeBoxElement(index).blur();
            }
            this.value = this.value + event.key;
        }
        if (eventCode == 8) {
            if (index == 1) {
                this.getCodeBoxElement(index).blur();
            } else {
                this.getCodeBoxElement(index - 1).focus();
            }
            this.value = this.value.substring(0, this.value.length-1)
        }
        event = new CustomEvent('onChange', { bubbles: true, composed: true, detail: {value: this.value}});
        this.dispatchEvent(event);
    }

    render() {
        const inputStyle = classMap({
            [this.class!]: this.class!,
            'title-bold': true,
            'error': this.isError === true
        });

        return html`
        ${this.arrDigit?.map((value) => {
            return html`
            <input id="codeBox${value}" class=${inputStyle} ?disabled=${this.disabled} type="number" maxlength="1" @keyup=${(event: any) => this.onKeyUpEvent(value, event)}/>
                `
        })}
        `
    }
}

declare global {
    interface HTMLElementTagNameMap {
        'signal-otp': SignalOTP
    }
}

export const SignalOTPReact = createComponent({
    tagName: 'signal-otp',
    elementClass: SignalOTP,
    react: React,
    events: {
        onChange: 'onChange',
    },
});