import { createComponent } from '@lit-labs/react';
import { css, html, LitElement, unsafeCSS } from 'lit';
import { classMap } from 'lit-html/directives/class-map.js';
import { customElement, property } from 'lit/decorators.js';
import React from 'react';
import globalStyles from '../../global.css?inline';
import styles from './chips.scss?inline';
import { when } from 'lit/directives/when.js';

@customElement('signal-chips')
export class SignalChips extends LitElement {
    static styles = [
        unsafeCSS(globalStyles),
        css`${unsafeCSS(styles)}`
    ]

    /**
    * Left icon chips.
    */
    @property()
    leftIcon: any;
    /**
    * Right icon chips.
    */
    @property()
    rightIcon: any;
    /**
    * Disabled chips.
    */
    @property({ type: Boolean })
    disabled?= false;
    /**
    * Selected chips.
    */
    @property({ type: Boolean })
    selected?= false;
    /**
    * Chips class.
    */
    @property({ type: String })
    class?= '';

    private rippleEffect(e: any) {
        let ripple = document.createElement("div")
        ripple.classList.add('ripple-primary')
        ripple.setAttribute("style", "top: " + e.offsetY + "px; left: " + e.offsetX + "px");
        e.target.appendChild(ripple)
    };

    private onClick(e: MouseEvent) {
        this.rippleEffect(e);
        const event = new CustomEvent('onClick', { bubbles: true, composed: true });
        this.dispatchEvent(event);
    }

    render() {
        const buttonStyle = classMap({
            [this.class!]: this.class!,
            'chips-tsel body03-regular': true,
            'selected': this.selected === true,
        });

        return html`
            <button class=${buttonStyle} ?disabled=${this.disabled} @click=${this.onClick}>
                <div class='w-auto flex flex-row items-center'>
                    ${when(this.leftIcon, () => html`
                        <div class="chips-icon-tsel">
                            <img src=${this.leftIcon} alt="leftIcon" />
                        </div>
                    `)}
                    <slot></slot>
                    ${when(this.rightIcon, () => html`
                        <div class="chips-icon-tsel">
                            <img src=${this.rightIcon} alt="rightIcon" />
                        </div>
                    `)}
                </div>
            </button>
        `
    }
}

declare global {
    interface HTMLElementTagNameMap {
        'signal-chips': SignalChips
    }
}

export const SignalChipsReact = createComponent({
    tagName: 'signal-chips',
    elementClass: SignalChips,
    react: React,
    events: {
        onClick: 'onClick',
    },
});