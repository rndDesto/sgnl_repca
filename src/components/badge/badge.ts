import { createComponent } from '@lit-labs/react';
import { css, html, LitElement, unsafeCSS } from 'lit';
import { classMap } from 'lit-html/directives/class-map.js';
import { customElement, property } from 'lit/decorators.js';
import React from 'react';
import globalStyles from '../../global.css?inline';
import styles from './badge.scss?inline';

@customElement('signal-badge')
export class SignalBadge extends LitElement {
    static styles = [
        unsafeCSS(globalStyles),
        css`${unsafeCSS(styles)}`
    ]

    /**
    * Variant button. Default is primary.
    */
    @property({ reflect: true })
    variant: 'primary' | 'secondary' = 'primary';
    /**
    * Badge class.
    */
    @property({ type: String })
    class?= '';

    render() {
        const badgeStyle = classMap({
            [this.class!]: this.class!,
            'badge-tsel': true,
            'badge-tsel-primary': this.variant == 'primary',
            'badge-tsel-secondary': this.variant === 'secondary',
        });

        return html`
        <div class="body01-bold">
            <span class="${badgeStyle} p-4">
                <slot></slot>
            </span>
        </div>
        `
    }
}

declare global {
    interface HTMLElementTagNameMap {
        'signal-badge': SignalBadge
    }
}

export const SignalBadgeReact = createComponent({
    tagName: 'signal-badge',
    elementClass: SignalBadge,
    react: React,
});