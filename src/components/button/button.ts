import { css, html, LitElement, unsafeCSS } from 'lit';
import { classMap } from 'lit-html/directives/class-map.js';
import { customElement, property } from 'lit/decorators.js';
import { ifDefined } from 'lit/directives/if-defined.js';
import { when } from 'lit/directives/when.js';
import globalStyles from '../../global.css?inline';
import styles from './button.scss?inline';

@customElement('signal-button')
export class SignalButton extends LitElement {

    static styles = [
        unsafeCSS(globalStyles),
        css`${unsafeCSS(styles)}`
    ]

    /**
    * Size button. Default is xl.
    */
    @property({ reflect: true })
    size: 'xl' | 'lg' | 'md' | 'sm' = 'xl';
    /**
    * Variant button. Default is primary.
    */
    @property({ reflect: true })
    variant: 'primary' | 'secondary' | 'text' = 'primary';
    /**
    * Disabled button. Default is false.
    */
    @property({ type: Boolean })
    disabled?= false
    /**
    * Left icon button.
    */
    @property({ type: String })
    leftIcon?= '';
    /**
    * Right icon button.
    */
    @property({ type: String })
    rightIcon?= '';
    /**
    * Button type. Default is button.
    */
    @property({ reflect: true })
    type: 'button' | 'reset' | 'submit' = 'button';
    /**
    * Button class.
    */
    @property({ type: String })
    class?= '';
    /**
    * Button classes.
    */
   
    @property({ type: String })
    partName?='';

    @property({ type: String })
    idName?='';

    @property({ type: String })
    testId?='';

    /** @nocollapse */
    static get formAssociated() {
        return true;
    }

    private readonly internals =
        (this as HTMLElement).attachInternals();

    private rippleEffect(e: any) {
        let primaryClassName = 'btn-tsel-primary';
        let secondaryClassName = 'btn-tsel-secondary';
        let ripple = document.createElement("div")

        if (e.target.classList.contains(primaryClassName)) {
            ripple.classList.add('ripple-primary')
            ripple.setAttribute("style", "top: " + e.offsetY + "px; left: " + e.offsetX + "px");
            e.target.appendChild(ripple)
        } else if (e.target.classList.contains(secondaryClassName)) {
            ripple.classList.add('ripple-secondary')
            ripple.setAttribute("style", "top: " + e.offsetY + "px; left: " + e.offsetX + "px");
            e.target.appendChild(ripple)
        }
    };

    private onClick(e: MouseEvent) {
        this.rippleEffect(e);
        const event = new CustomEvent('onClick', { bubbles: true, composed: true });
        this.dispatchEvent(event);

        const { internals: { form } } = this;
        if (!form) {
            return;
        }
        const isSubmit = this.type === 'submit', isReset = this.type === 'reset';
        if (!(isSubmit || isReset)) {
            return;
        }
        e.stopPropagation();
        if (isSubmit) {
            form.requestSubmit();
        } else if (isReset) {
            form.reset();
        }
    }

    render() {
        const buttonStyle = classMap({
            [this.class!]: this.class!,
            'body01-bold relative z-[1]': true,
            'btn-tsel-primary': this.variant === 'primary',
            'btn-tsel-secondary': this.variant === 'secondary',
            'btn-tsel-text': this.variant === 'text',
            'btn-tsel-xl': this.size === 'xl',
            'btn-tsel-lg': this.size === 'lg',
            'btn-tsel-md': this.size === 'md',
            'btn-tsel-sm': this.size === 'sm',
        });

        return html`
        <button 
            data-testid=${this.testId === '' ?'tsel-testid': this.testId}
            part=${this.partName === '' ?'tselpart': this.partName}
            id=${this.idName === '' ?'tselid': this.idName} 
            class=${buttonStyle} 
            ?disabled=${this.disabled} 
            @click=${this.onClick}>
            ${when(this.leftIcon != '', () => html`<img class="mr-4" src=${ifDefined(this.leftIcon)} alt="Icon" height="24" width="24" />`)}
            <span>
                <slot></slot>
            </span>
            ${when(this.rightIcon != '', () => html`<img class="ml-4" src=${ifDefined(this.rightIcon)} alt="Icon" height="24" width="24" />`)}
        </button>
        `
    }
}

declare global {
    interface HTMLElementTagNameMap {
        'signal-button': SignalButton
    }
}