import React from "react";
import { SignalButton } from "./button";
import { createComponent } from "@lit-labs/react";

const SignalButtonReact = createComponent({
    tagName: 'signal-button',
    elementClass: SignalButton,
    react: React,
    events: {
        onClick: 'onClick',
    },
});

export default SignalButtonReact