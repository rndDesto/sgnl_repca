import { css, html, LitElement, unsafeCSS } from 'lit';
import { classMap } from 'lit-html/directives/class-map.js';
import { customElement, property } from 'lit/decorators.js';
import globalStyles from '@/global.css?inline';
import styles from './title.scss?inline';

@customElement('signal-title')
export class SignalTitle extends LitElement {

    static styles = [
        unsafeCSS(globalStyles),
        css`${unsafeCSS(styles)}`
    ]
    
    @property({ type: String })
    partName?='';

    @property({ type: String })
    idName?='';

    @property({ type: String })
    testId?='';

    @property({ type: String })
    weight?: 'bold' | 'semi'| 'normal' = 'normal';


    render() {
        const weightClass = this.weight ?`f-${this.weight}`:'';

        const styledClasses = classMap({
            'title-signal': true,
            [weightClass]: !!this.weight
        });

        return html`
        <div 
            data-testid=${this.testId === '' ?'data-test-titleid': this.testId}
            part=${this.partName === '' ?'tselparttitle': this.partName}
            id=${this.idName === '' ?'tsel-id-title': this.idName} 
            class=${styledClasses}
            >
                <slot></slot>
        </div>
        `
    }
}

declare global {
    interface HTMLElementTagNameMap {
        'signal-title': SignalTitle
    }
}