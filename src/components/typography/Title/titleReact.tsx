import React from "react";
import { SignalTitle } from "./title";
import { createComponent } from "@lit-labs/react";

const SignalTitleReact = createComponent({
    tagName: 'signal-button',
    elementClass: SignalTitle,
    react: React,
    events: {
        onClick: 'onClick',
    },
});

export default SignalTitleReact