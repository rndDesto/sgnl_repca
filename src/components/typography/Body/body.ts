import { css, html, LitElement, unsafeCSS } from 'lit';
import { classMap } from 'lit-html/directives/class-map.js';
import { customElement, property } from 'lit/decorators.js';
import globalStyles from '@/global.css?inline';
import styles from './body.scss?inline';

@customElement('signal-body')
export class SignalBody extends LitElement {

    static styles = [
        unsafeCSS(globalStyles),
        css`${unsafeCSS(styles)}`
    ]
    
    @property({ type: String })
    partName?='';

    @property({ type: String })
    idName?='';

    @property({ type: String })
    testId?='';

    @property()
    size?: string | number;

    @property({ type: String })
    weight?: 'bold' | 'semi'| 'normal' = 'normal';

    render() {
        const sizeClass = this.size ?`size-${this.size}`:'';
        const weightClass = this.weight ?`f-${this.weight}`:'';

        const styledClasses = classMap({
            'body-signal': true,
            [sizeClass]: !!this.size,
            [weightClass]: !!this.weight
        });

        return html`
        <div 
            data-testid=${this.testId === '' ?'data-test-bodyid': this.testId}
            part=${this.partName === '' ?'tselpartbody': this.partName}
            id=${this.idName === '' ?'tsel-id-body': this.idName} 
            class=${styledClasses}
            >
                <slot></slot>
        </div>
        `
    }
}

declare global {
    interface HTMLElementTagNameMap {
        'signal-body': SignalBody
    }
}