import React from "react";
import { SignalBody } from "./body";
import { createComponent } from "@lit-labs/react";

const SignalBodyReact = createComponent({
    tagName: 'signal-button',
    elementClass: SignalBody,
    react: React,
    events: {
        onClick: 'onClick',
    },
});

export default SignalBodyReact