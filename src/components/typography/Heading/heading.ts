import { css, html, LitElement, unsafeCSS } from 'lit';
import { classMap } from 'lit-html/directives/class-map.js';
import { customElement, property } from 'lit/decorators.js';
import globalStyles from '@/global.css?inline';
import styles from './heading.scss?inline';

@customElement('signal-heading')
export class SignalHeading extends LitElement {

    static styles = [
        unsafeCSS(globalStyles),
        css`${unsafeCSS(styles)}`,
    ]
    
    @property({ type: String })
    partName?='';

    @property({ type: String })
    idName?='';

    @property({ type: String })
    testId?='';

    @property()
    size?: string | number;

    @property()
    classes?: string;

    @property({ type: String })
    weight?: 'bold' | 'semi'| 'normal' = 'normal';
    
    @property({ type: String })
    color?: 'primary' | 'secondary' | 'tritary' | 'valid'  | 'info' | 'warning' | 'error' | 'disable' = 'primary';


    render() {
        const sizeClass = this.size ?`size-${this.size}`:'';
        const weightClass = this.weight ?`f-${this.weight}`:'';
        const classesProps = this.classes ?`${this.classes}`:'';
        const colorProps = this.color ?`text-${this.color}`:'';

        const styledClasses = classMap({
            [classesProps]:true,
            'heading-signal': true,
            [sizeClass]: !!this.size,
            [weightClass]: !!this.weight,
            [colorProps]: !!this.color,
        });

        return html`
        <div 
            data-testid=${this.testId === '' ?'data-test-headingid': this.testId}
            part=${this.partName === '' ?'tselpartheading': this.partName}
            id=${this.idName === '' ?'tsel-id-heading': this.idName} 
            class=${styledClasses}
            >
                <slot></slot>
        </div>
        `
    }
}

declare global {
    interface HTMLElementTagNameMap {
        'signal-heading': SignalHeading
    }
}