import React from "react";
import { SignalHeading } from "./heading";
import { createComponent } from "@lit-labs/react";

const SignalHeadingReact = createComponent({
    tagName: 'signal-button',
    elementClass: SignalHeading,
    react: React,
    events: {
        onClick: 'onClick',
    },
});

export default SignalHeadingReact