import { createComponent } from '@lit-labs/react';
import { css, html, LitElement, unsafeCSS } from 'lit';
import { classMap } from 'lit-html/directives/class-map.js';
import { customElement, property } from 'lit/decorators.js';
import React from 'react';
import globalStyles from '../../global.css?inline';
import styles from './tab.scss?inline';
import { when } from 'lit/directives/when.js';
import { ifDefined } from 'lit/directives/if-defined.js';

@customElement('signal-tab')
export class SignalTab extends LitElement {
    static styles = [
        unsafeCSS(globalStyles),
        css`${unsafeCSS(styles)}`
    ]
    /**
    * Selected tab.
    */
    @property({ type: Boolean })
    selected?= false;
    /**
    * Icon tab.
    */
    @property({ type: String })
    icon?: '';
    /**
    * Label tab.
    */
    @property({ type: String })
    label?= '';
    /**
    * Label tab.
    */
    @property({ type: Boolean })
    isCustomIcon?= false;
    /**
    * Tab class.
    */
    @property({ type: String })
    class?= '';

    render() {
        const labelStyle = classMap({
            [this.class!]: this.class!,
            'body01-bold tab-text': true,
            'text-primary': !this.selected,
            'text-tertiary': this.selected === true && this.isCustomIcon === true,
            'text-secondary': !this.selected
        });

        const indicatorStyle = classMap({
            'indicator': !this.isCustomIcon,
            'custom-indicator': this.isCustomIcon === true,
        });

        return html`
            <button class="tab-container">
                ${when(this.isCustomIcon, () => html`
                    <img class="tab-icon" src=${ifDefined(this.icon)} alt="Tab Icon" />
                `)}
                <p class=${labelStyle}>${this.label}</p>
                ${when(this.selected, () => html`<div class=${indicatorStyle}></div>`)}
            </button>
        `
    }
}

declare global {
    interface HTMLElementTagNameMap {
        'signal-tab': SignalTab
    }
}

export const SignalTabReact = createComponent({
    tagName: 'signal-tab',
    elementClass: SignalTab,
    react: React,
    events: {
        onClick: 'onClick',
    },
});