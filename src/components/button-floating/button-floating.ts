import { createComponent } from '@lit-labs/react';
import { css, html, LitElement, unsafeCSS } from 'lit';
import { CartIcon } from '../../assets';
import { customElement, property } from 'lit/decorators.js';
import { when } from 'lit/directives/when.js';
import React from 'react';
import globalStyles from '../../global.css?inline';
import styles from './button-floating.scss?inline';
import { ifDefined } from 'lit/directives/if-defined.js';

@customElement('signal-button-floating')
export class SignalButtonFloating extends LitElement {
    static styles = [
        unsafeCSS(globalStyles),
        css`${unsafeCSS(styles)}`
    ]

    /**
    * Icon fab.
    */
    @property({ type: String })
    icon?= '';
    /**
    * Badge fab.
    */
    @property({ type: String })
    badge?= '';
    /**
    * FAB class.
    */
    @property({ type: String })
    class?= '';

    private onClick() {
        const event = new CustomEvent('onClick', { bubbles: true, composed: true });
        this.dispatchEvent(event);
    }

    render() {
        return html`
        <button class='btn-tsel-floating-icon ${this.class}' @click=${this.onClick}>
            ${when(this.badge != '', () => html`<div class='floating-badge body02-bold'>${this.badge}</div>`)}
            <img src=${ifDefined(this.icon ? this.icon : CartIcon)} alt="Icon" height=35 width=35 />
        </button>
        `
    }
}

declare global {
    interface HTMLElementTagNameMap {
        'signal-button-floating': SignalButtonFloating
    }
}

export const SignalButtonFloatingReact = createComponent({
    tagName: 'signal-button-floating',
    elementClass: SignalButtonFloating,
    react: React,
    events: {
        onClick: 'onClick',
    },
});