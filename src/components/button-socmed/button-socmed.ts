import { createComponent } from '@lit-labs/react';
import { AppleIcon, FacebookIcon, GoogleIcon, PlaceholderIcon, TwitterIcon } from '../../assets';
import { css, html, LitElement, unsafeCSS } from 'lit';
import { classMap } from 'lit-html/directives/class-map.js';
import { customElement, property } from 'lit/decorators.js';
import React from 'react';
import globalStyles from '../../global.css?inline';
import styles from './button-socmed.scss?inline';

@customElement('signal-button-socmed')
export class SignalButtonSocmed extends LitElement {
    static styles = [
        unsafeCSS(globalStyles),
        css`${unsafeCSS(styles)}`
    ]

    /**
    * Variant button. Default is apple.
    */
    @property({ reflect: true })
    variant: 'apple' | 'google' | 'facebook' | 'twitter' = 'apple';
    /**
    * Disabled button. Default is false.
    */
    @property({ type: Boolean })
    disabled?= false;
    /**
    * Socmed Button class.
    */
    @property({ type: String })
    class?= '';

    private onClick() {
        const event = new CustomEvent('onClick', { bubbles: true, composed: true });
        this.dispatchEvent(event);
    }

    render() {
        const buttonStyle = classMap({
            [this.class!]: this.class!,
            'btn-tsel-apple': this.variant === 'apple',
            'btn-tsel-google': this.variant === 'google',
            'btn-tsel-facebook': this.variant === 'facebook',
            'btn-tsel-twitter': this.variant === 'twitter',
        });

        const socmedIcon = () => {
            switch (this.variant) {
                case 'apple':
                    return AppleIcon;
                case 'google':
                    return GoogleIcon;
                case 'facebook':
                    return FacebookIcon;
                case 'twitter':
                    return TwitterIcon;
                default:
                    return PlaceholderIcon;
            }
        };

        const socmedText = () => {
            switch (this.variant) {
                case 'apple':
                    return 'Apple';
                case 'google':
                    return 'Google';
                case 'facebook':
                    return 'Facebook';
                case 'twitter':
                    return 'Twitter';
                default:
                    return 'Unknown';
            }
        };


        return html`
            <button class=${buttonStyle} ?disabled=${this.disabled} @click=${this.onClick}>
            <div class="w-full flex flex-row items-center justify-center">
                <div class="w-[24px] h-[24px] flex items-center justify-center ml-[8px]">
                    <img src=${socmedIcon()} alt="Icon" height={16} width={16} />
                </div>
                <p class="title-bold ml-[8px]">${`Sign in with ${socmedText()}`}</p>
            </div>
            </button>
        `
    }
}

declare global {
    interface HTMLElementTagNameMap {
        'signal-button-socmed': SignalButtonSocmed
    }
}

export const SignalButtonSocmedReact = createComponent({
    tagName: 'signal-button-socmed',
    elementClass: SignalButtonSocmed,
    react: React,
    events: {
        onClick: 'onClick',
    },
});