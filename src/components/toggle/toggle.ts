import { createComponent } from '@lit-labs/react';
import { css, html, LitElement, unsafeCSS } from 'lit';
import { customElement, property } from 'lit/decorators.js';
import React from 'react';
import globalStyles from '../../global.css?inline';
import styles from './toggle.scss?inline';
import { classMap } from 'lit-html/directives/class-map.js';
import { when } from 'lit/directives/when.js';
import { ifDefined } from 'lit/directives/if-defined.js';

@customElement('signal-toggle')
export class SignalToggle extends LitElement {
    static styles = [
        unsafeCSS(globalStyles),
        css`${unsafeCSS(styles)}`
    ]

    /**
    * Checked radio.
    */
    @property({ type: Boolean })
    checked?= false;
    /**
    * Disabled radio.
    */
    @property({ type: Boolean })
    disabled?= false;
    /**
    * Toggle class.
    */
    @property({ type: String })
    class?= '';
    /**
    * Toggle label position.
    */
    @property({ reflect: true })
    labelPosition: 'left' | 'right' = 'right';
    /**
    * Toggle value.
    */
    @property({ type: String })
    value?= '';


    private onChange() {
        let customEvent = new CustomEvent('onChange', { bubbles: true, composed: false, detail: { value: this.value } });
        this.dispatchEvent(customEvent);
    }

    render() {
        const containerStyle = classMap({
            'container-switch': true,
            'body01-regular': this.checked === false,
            'body01-bold': this.checked === true
        });

        const toggleStyle = classMap({
            [this.class!]: this.class!,
            'switch-tsel': true,
            'switch-left': this.labelPosition == 'left',
            'switch-right': this.labelPosition == 'right',
        });

        return html`
        <div class=${containerStyle}>
            ${when(this.labelPosition == 'left', () => html`<slot></slot>`)}
            <label class=${toggleStyle}>
                <input type="checkbox" value=${ifDefined(this.value)} ?checked=${this.checked} ?disabled=${this.disabled} @click=${this.onChange}/>
                <span class="slider round"></span>
            </label>
            ${when(this.labelPosition == 'right', () => html`<slot></slot>`)}
        </div>
        `
    }
}

declare global {
    interface HTMLElementTagNameMap {
        'signal-toggle': SignalToggle
    }
}

export const SignalToggleReact = createComponent({
    tagName: 'signal-toggle',
    elementClass: SignalToggle,
    react: React,
    events: {
        onChange: 'onChange',
    },
});