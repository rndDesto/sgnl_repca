import { createComponent } from '@lit-labs/react';
import { css, html, LitElement, unsafeCSS } from 'lit';
import { customElement, property } from 'lit/decorators.js';
import { when } from 'lit/directives/when.js';
import React from 'react';
import globalStyles from '../../global.css?inline';
import styles from './snackbar.scss?inline';

@customElement('signal-snackbar')
export class SignalSnackbar extends LitElement {
    static styles = [
        unsafeCSS(globalStyles),
        css`${unsafeCSS(styles)}`
    ]
    /**
    * Snackbar message.
    */
    @property({ type: String })
    variant: 'general' | 'warning' = 'general';
    /**
    * Text button message.
    */
    @property({ type: String })
    textButton?= '';
    /**
    * Snackbar duration.
    */
    @property({ type: Number })
    duration: 3000 | 5000 = 3000;
    /**
    * Snackbar show state.
    */
    @property({ type: Boolean })
    show?= false;
    /**
    * Snackbar class.
    */
    @property({ type: String })
    class?= '';

    update(changedProperties: Map<string, unknown>) {
        if (changedProperties.has("show")) {
            const oldValue = changedProperties.get("show") as boolean
            if (oldValue === false) {
                this.onClick();
            }
        }
        super.update(changedProperties);
    }

    private onClick() {
        const x = this.shadowRoot?.getElementById("snackbar") as HTMLDivElement;
        x.className = "show";
        x.classList.add('snackbar-tsel')
        if (this.variant == 'general') {
            x.classList.add('snackbar-tsel-general')
        } else {
            x.classList.add('snackbar-tsel-warning')
        }
        setTimeout(function () {
            x.className = x.className.replace("show", "");
        }, this.duration);
    }

    private onClickTextButton() {
        const x = this.shadowRoot?.getElementById("snackbar") as HTMLDivElement;
        x.className = "";
        const event = new CustomEvent('onClickTextButton', { bubbles: true, composed: true });
        this.dispatchEvent(event);
    }

    render() {
        return html`
        <div id="snackbar" class="body01-regular ${this.class}">
            <p>
                <slot></slot>
            </p>
            ${when(this.textButton != '', () => html`<div class="snackbar-tsel-textbutton body01-bold" @click=${this.onClickTextButton}>${this.textButton}</div>`)}
        </div>
        `
    }
}

declare global {
    interface HTMLElementTagNameMap {
        'signal-snackbar': SignalSnackbar
    }
}

export const SignalSnackbarReact = createComponent({
    tagName: 'signal-snackbar',
    elementClass: SignalSnackbar,
    react: React,
    events: {
        onClickTextButton: 'onClickTextButton',
    },
});