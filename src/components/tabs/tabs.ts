import { createComponent } from '@lit-labs/react';
import { css, html, LitElement, unsafeCSS } from 'lit';
import { customElement, property } from 'lit/decorators.js';
import React from 'react';
import globalStyles from '../../global.css?inline';
import styles from './tabs.scss?inline';
@customElement('signal-tabs')
export class SignalTabs extends LitElement {
    static styles = [
        unsafeCSS(globalStyles),
        css`${unsafeCSS(styles)}`
    ]
    /**
    * OTP class.
    */
    @property({ type: String })
    class?= '';

    render() {
        return html`
            <div class="w-full flex flex-row items-center container-tabs ${this.class}">
                <slot></slot>
            </div>
        `
    }
}

declare global {
    interface HTMLElementTagNameMap {
        'signal-tabs': SignalTabs
    }
}

export const SignalTabsReact = createComponent({
    tagName: 'signal-tabs',
    elementClass: SignalTabs,
    react: React,
});