import { createComponent } from '@lit-labs/react';
import { css, html, LitElement, unsafeCSS } from 'lit';
import { customElement, property, state } from 'lit/decorators.js';
import React from 'react';
import globalStyles from '../../global.css?inline';
import styles from './textfield.scss?inline';
import { classMap } from 'lit-html/directives/class-map.js';
import { when } from 'lit/directives/when.js';
import { EyeCloseIcon, EyeOpenIcon, InfoIcon } from '../../assets';
import { ifDefined } from 'lit/directives/if-defined.js';

@customElement('signal-textfield')
export class SignalTextfield extends LitElement {
    static styles = [
        unsafeCSS(globalStyles),
        css`${unsafeCSS(styles)}`
    ]

    /**
    * Textfield label.
    */
    @property({ type: String })
    label?= 'Label'
    /**
    * Flag for error state.
    */
    @property({ type: Boolean })
    isError?= false
    /**
    * Error text.
    */
    @property({ type: String })
    errorText?= 'Error text'
    /**
    * Flag for valid state.
    */
    @property({ type: Boolean })
    isValid?= false
    /**
    * Valid text.
    */
    @property({ type: String })
    validText?= 'Valid text'
    /**
    * Flag for loading state.
    */
    @property({ type: Boolean })
    isLoading?= false
    /**
    * Disabled textfield.
    */
    @property({ type: Boolean })
    disabled?= false
    /**
    * Flag for show info state.
    */
    @property({ type: Boolean })
    showInfo?= false
    /**
    * Flag for required state.
    */
    @property({ type: Boolean })
    required?= false
    /**
    * Placeholder textfield.
    */
    @property({ type: String })
    placeholder?= 'Placeholder'
    /**
    * Flag for password state.
    */
    @property({ type: Boolean })
    obscureText?= false
    /**
    * Value textfield.
    */
    @property({ type: String })
    value?= ''
    /**
    * Textfield class.
    */
    @property({ type: String })
    class?= '';
    /**
    * Textfield type.
    */
    @property({ type: String })
    type: 'text' | 'password' | 'email' | 'number' | 'tel' = 'text';
    /**
    * Max length value text field.
    */
    @property({ type: Number })
    maxLength?= 200
    /**
    * Min length value text field.
    */
    @property({ type: Number })
    minLength?= 0

    @state()
    showPassword = true;

    private showText() {
        this.showPassword = !this.showPassword;
        if (this.showPassword === false) {
            this.type = 'text'
        } else {
            this.type = 'password'
        }
    }

    private onClickIconInfo() {
        const event = new CustomEvent('onClickIconInfo', { bubbles: true, composed: true });
        this.dispatchEvent(event);
    }

    private onChange(event: any) {
        this.value = event.target.value;
        let customEvent = new CustomEvent('onChange', { bubbles: true, composed: true, detail: { value: this.value } });
        this.dispatchEvent(customEvent);
    }

    private onError() {
        let inputId = this.shadowRoot?.getElementById('input') as HTMLInputElement;
        this.value = inputId.value;
        let customEvent = new CustomEvent('onError', { bubbles: true, composed: true, detail: { value: this.value } });
        this.dispatchEvent(customEvent);
    }

    private onSubmit() {
        let inputId = this.shadowRoot?.getElementById('input') as HTMLInputElement;
        this.value = inputId.value;
        let customEvent = new CustomEvent('onSubmit', { bubbles: true, composed: true, detail: { value: this.value } });
        this.dispatchEvent(customEvent);
    }

    render() {
        const inputStyles = classMap({
            'input-style body02-regular': true,
            'input-loading': this.isLoading === true,
            'input-error': this.isError === true && this.isValid === false,
            'input-success': this.isValid === true && this.isError === false,
        });

        return html`
        <div class="input-container ${this.class}">
            ${when(this.label, () => html`
                <div class="flex flex-row items-center">
                <p class="body02-regular text-primary">
                    ${this.label}
                    ${when(this.required, () => html`<span>*</span>`)}
                </p>
                ${when(this.showInfo, () => html`
                    <button class="w-5 h-5 ml-1" @click=${this.onClickIconInfo}>
                        <img src=${InfoIcon} alt="Info Icon" />
                    </button>
                `)}
                </div>`
        )}
            <div class="relative">
                <input id="input" @keyup=${this.onChange} type=${this.type} class=${inputStyles} placeholder=${ifDefined(this.placeholder)} ?disabled=${this.disabled} value=${ifDefined(this.value)} maxlength=${ifDefined(this.maxLength)} minlength=${ifDefined(this.minLength)} @error=${this.onError} @submit=${this.onSubmit}>
                ${when(this.obscureText && !this.disabled, () => html`<button class="password-button" @click=${this.showText}>
                    <img src=${this.showPassword ? EyeCloseIcon : EyeOpenIcon} alt="password" />
                    </button>`)}
            </div>
            <div class="body02-regular mt-[4px]">
                ${when(this.isError && !this.isValid, () => html`<p class="error-text">${this.errorText}</p>`)}
                ${when(this.isValid && !this.isError, () => html`<p class="valid-text">${this.validText}</p>`)}
            </div>
        </div>
        `
    }
}

declare global {
    interface HTMLElementTagNameMap {
        'signal-textfield': SignalTextfield
    }
}

export const SignalTextfieldReact = createComponent({
    tagName: 'signal-textfield',
    elementClass: SignalTextfield,
    react: React,
    events: {
        onChange: 'onChange',
        onClickIconInfo: 'onClickIconInfo',
        onError: 'onError',
        onSubmit: 'onSubmit'
    },
});