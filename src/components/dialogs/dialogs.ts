import { createComponent } from '@lit-labs/react';
import { CookiesIcon } from '../../assets';
import { classMap } from 'lit-html/directives/class-map.js';
import { css, html, LitElement, unsafeCSS } from 'lit';
import { customElement, property } from 'lit/decorators.js';
import React from 'react';
import globalStyles from '../../global.css?inline';
import styles from './dialogs.scss?inline';
import '../button/button.ts';

@customElement('signal-dialogs')
export class SignalDialogs extends LitElement {
    static styles = [
        unsafeCSS(globalStyles),
        css`${unsafeCSS(styles)}`
    ]
    /**
    * Dialogs class.
    */
    @property({ type: String })
    class?= '';

    private onClickSecondaryButton() {
        const event = new CustomEvent('onClickSecondaryButton', { bubbles: true, composed: true });
        this.dispatchEvent(event);
    }

    private onClickPrimaryButton() {
        const event = new CustomEvent('onClickPrimaryButton', { bubbles: true, composed: true });
        this.dispatchEvent(event);
    }

    render() {
        const dialogsTextStyle = classMap({
            'body01-regular dialogs-text-tsel': true,
        })

        return html`
        <div class="dialogs-tsel ${this.class}">
            <img class="dialogs-icon-tsel" src=${CookiesIcon} height="40" width="40">
            <div class=${dialogsTextStyle}>
                <slot></slot>
            </div>
            <div class="dialogs-button-tsel">
                <div class="dialogs-button-item-tsel button-secondary">
                    <signal-button variant="secondary" @click=${this.onClickSecondaryButton}>Nanti Saja</signal-button>
                </div>
                <div class="dialogs-button-item-tsel">
                    <signal-button variant="primary" @click=${this.onClickPrimaryButton}>Lanjutkan</signal-button>
                </div>
            </div>
        </div>
        `
    }
}

declare global {
    interface HTMLElementTagNameMap {
        'signal-dialogs': SignalDialogs
    }
}

export const SignalDialogsReact = createComponent({
    tagName: 'signal-dialogs',
    elementClass: SignalDialogs,
    react: React,
    events: {
        onClickPrimaryButton: 'onClick',
        onClickSecondaryButton: 'onClickSecondaryButton'
    },
});