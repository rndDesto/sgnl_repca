import { createComponent } from '@lit-labs/react';
import { css, html, LitElement, unsafeCSS } from 'lit';
import { classMap } from 'lit-html/directives/class-map.js';
import { customElement, property } from 'lit/decorators.js';
import React from 'react';
import globalStyles from '../../global.css?inline';
import styles from './language-selector.scss?inline';

@customElement('signal-language-selector')
export class SignalLanguageSelector extends LitElement {
    static styles = [
        unsafeCSS(globalStyles),
        css`${unsafeCSS(styles)}`
    ]

    /**
    * Country variant. Default is id.
    */
    @property({ reflect: true })
    selectedLanguage: 'id' | 'en' = 'id';
    /**
    * Language selector chevron.
    */
    @property({ type: Boolean })
    chevron?= false;
    /**
    * Language selector class.
    */
    @property({ type: String })
    class?= '';

    private onChange() {
        const event = new CustomEvent('onChange', { bubbles: true, composed: true });
        this.dispatchEvent(event);
    }

    render() {
        let label = '';
        let flagIcon = '';
        switch (this.selectedLanguage) {
            case 'id':
                label = 'ID';
                flagIcon = 'indonesia';
                break;
            case 'en':
                label = 'US'
                flagIcon = 'us';
                break;
            default:
                break;
        }

        const languageSelectorStyle = classMap({
            [this.class!]: this.class!,
            'language-selector label-bold': true,
        });

        const chevronStyle = classMap({
            'chevron-right-hide': this.chevron === false,
            'chevron-right': this.chevron === true

        })

        return html`
        <div class=${languageSelectorStyle}  @click=${this.onChange}>
            <span class="icon-country-${flagIcon}"></span>
            <div class="mr-4"></div>
            <p>${label}</p>
            <span class=${chevronStyle}></span>
        </div>
        `
    }
}

declare global {
    interface HTMLElementTagNameMap {
        'signal-language-selector': SignalLanguageSelector
    }
}

export const SignalLanguageSelectorReact = createComponent({
    tagName: 'signal-language-selector',
    elementClass: SignalLanguageSelector,
    react: React,
    events: {
        onChange: 'onChange',
    },
});