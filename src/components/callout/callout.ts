import { createComponent } from '@lit-labs/react';
import { css, html, LitElement, unsafeCSS } from 'lit';
import { classMap } from 'lit-html/directives/class-map.js';
import { customElement, property } from 'lit/decorators.js';
import { when } from 'lit/directives/when.js';
import React from 'react';
import { CloseIcon, InfoIcon, WarningIcon } from '../../assets';
import globalStyles from '../../global.css?inline';
import styles from './callout.scss?inline';

@customElement('signal-callout')
export class SignalCallout extends LitElement {
    static styles = [
        unsafeCSS(globalStyles),
        css`${unsafeCSS(styles)}`
    ]

    /**
    * Callout title.
    */
    @property({ type: String })
    titleMessage?= 'Title Message';
    /**
    * Callout message.
    */
    @property({ type: String })
    message?= 'Info message';
    /**
    * Callout text button.
    */
    @property({ type: String })
    textButton?= 'Action';
    /**
    * Callout type.
    */
    @property({ reflect: true })
    type: 'info' | 'warning' = 'info';
    /**
    * Callout type.
    */
    @property({ type: Boolean })
    closeable?= false;
    /**
    * Callout class.
    */
    @property({ type: String })
    class?= '';

    private onClickAction() {
        const event = new CustomEvent('onClickAction', { bubbles: true, composed: true });
        this.dispatchEvent(event);
    }

    private onClose() {
        const event = new CustomEvent('onClose', { bubbles: true, composed: true });
        this.dispatchEvent(event);
    }

    render() {
        const Icon = () => {
            switch (this.type) {
                case 'info':
                    return InfoIcon;
                case 'warning':
                    return WarningIcon;
                default:
                    return InfoIcon;
            }
        };

        const containerStyle = classMap({
            [this.class!]: this.class!,
            'callout-tsel': true,
            'info': this.type === 'info',
            'warning': this.type === 'warning',
        });

        return html`
        <div class=${containerStyle}>
            <div class="flex flex-row">
                <img class="callout-tsel-icon" src=${Icon()} alt="icon" />
                <div class="callout-tsel-text">
                    <p class="title-bold">${this.titleMessage}</p>
                    <p class="body01-regular">${this.message}</p>
                    ${when(this.textButton != null, () => html`<button class="text-info title-bold" @click=${this.onClickAction}>${this.textButton}</button>`)}
                </div>
            </div>
            ${when(this.closeable, () => html`
                <button class="callout-tsel-icon" @click=${this.onClose}>
                    <img width="20" height="20" src=${CloseIcon} alt="icon"/>
                    </button>
            `)}
        </div>
        `
    }
}

declare global {
    interface HTMLElementTagNameMap {
        'signal-callout': SignalCallout
    }
}

export const SignalCalloutReact = createComponent({
    tagName: 'signal-callout',
    elementClass: SignalCallout,
    react: React,
    events: {
        onClickAction: 'onClickAction',
        onClose: 'onClose'
    },
});