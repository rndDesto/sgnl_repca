import { createComponent } from '@lit-labs/react';
import { css, html, LitElement, unsafeCSS } from 'lit';
import { classMap } from 'lit-html/directives/class-map.js';
import { customElement, property } from 'lit/decorators.js';
import React from 'react';
import globalStyles from '../../global.css?inline';
import styles from './banner.scss?inline';
import { ifDefined } from 'lit/directives/if-defined.js';

@customElement('signal-banner')
export class SignalBanner extends LitElement {
    static styles = [
        unsafeCSS(globalStyles),
        css`${unsafeCSS(styles)}`,
    ]

    /**
    * Variant banner.
    */
    @property({ reflect: true })
    variant: 'brand-sm' | 'brand-lg' | 'rectangle-sm' | 'rectangle-md' | 'rectangle-lg' | 'square-sm' | 'square-lg' = 'rectangle-sm';
    /**
    * Image banner.
    */
    @property({ type: String })
    image?= '';
    /**
    * Image width.
    */
    @property({ type: String })
    width?= '';
    /**
    * Image height.
    */
    @property({ type: String })
    height?= '';
    /**
    * Banner class.
    */
    @property({ type: String })
    class?= '';

    private onClick() {
        const event = new CustomEvent('onClick', { bubbles: true, composed: true });
        this.dispatchEvent(event);
    }

    render() {
        const bannerStyle = classMap({
            [this.class!]: this.class!,
            'object-cover': true,
            'banner-tsel-brand-sm': this.variant === 'brand-sm',
            'banner-tsel-brand-lg': this.variant === 'brand-lg',
            'banner-tsel-rectangle-sm': this.variant === 'rectangle-sm',
            'banner-tsel-rectangle-md': this.variant === 'rectangle-md',
            'banner-tsel-rectangle-lg': this.variant === 'rectangle-lg',
            'banner-tsel-square-sm': this.variant === 'square-sm',
            'banner-tsel-square-lg': this.variant === 'square-lg',
        });

        return html`
        <button @click=${this.onClick} type="button">
            <img class=${bannerStyle} src=${ifDefined(this.image)} alt="banner image" style="width:${this.width}px;height:${this.height}px"/>
        </button>
        `
    }
}

declare global {
    interface HTMLElementTagNameMap {
        'signal-banner': SignalBanner
    }
}

export const SignalBannerReact = createComponent({
    tagName: 'signal-banner',
    elementClass: SignalBanner,
    react: React,
    events: {
        onClick: 'onClick',
    },
});