import { createComponent } from '@lit-labs/react';
import { css, html, LitElement, unsafeCSS } from 'lit';
import { classMap } from 'lit-html/directives/class-map.js';
import { customElement, property } from 'lit/decorators.js';
import React from 'react';
import globalStyles from '../../global.css?inline';
import { CartIcon } from '../../assets';
import styles from './button-icon.scss?inline';
import { ifDefined } from 'lit/directives/if-defined.js';

@customElement('signal-button-icon')
export class SignalButtonIcon extends LitElement {
    static styles = [
        unsafeCSS(globalStyles),
        css`${unsafeCSS(styles)}`
    ]

    /**
    * Variant icon button. Default is primary.
    */
    @property({ reflect: true })
    variant: 'primary' | 'secondary' = 'primary';
    /**
    * Disabled icon button. Default is false.
    */
    @property({ type: Boolean })
    disabled?= false
    /**
    * Icon of icon button.
    */
    @property({ type: String })
    icon?= '';
    /**
    * Icon button class.
    */
    @property({ type: String })
    class?= '';

    private onClick() {
        const event = new CustomEvent('onClick', { bubbles: true, composed: true });
        this.dispatchEvent(event);
    }

    render() {
        const buttonStyle = classMap({
            [this.class!]: this.class!,
            'icon-button-tsel': true,
            'icon-button-tsel-primary': this.variant == 'primary',
            'icon-button-tsel-secondary': this.variant === 'secondary',
        });

        return html`
        <button class=${buttonStyle} @click=${this.onClick} ?disabled=${this.disabled}>
            <img class="icon-calendar" src=${ifDefined(this.icon ? this.icon : CartIcon)} alt="Icon"/>
        </button>
        `
    }
}

declare global {
    interface HTMLElementTagNameMap {
        'signal-button-icon': SignalButtonIcon
    }
}

export const SignalButtonIconReact = createComponent({
    tagName: 'signal-button-icon',
    elementClass: SignalButtonIcon,
    react: React,
    events: {
        onClick: 'onClick',
    },
});