

## 0.0.151 (2023-08-18)


### Bug Fixes

* ganti nama paket ([198168f](https://gitlab.com/rndDesto/sgnl_repca/commit/198168f2bef9f7fdbd370a7b82b4d1ef553aa401))

## 0.0.150 (2023-08-18)


### Bug Fixes

* confign vite ([7a3575d](https://gitlab.com/rndDesto/sgnl_repca/commit/7a3575d9a690f0fc2c6af8ee17616f41b91c8ede))
* ganti module ([eb56cb8](https://gitlab.com/rndDesto/sgnl_repca/commit/eb56cb80a2a8cc310a2b99071cb81e1b00fe3d60))
* tailwind config ([363e3b9](https://gitlab.com/rndDesto/sgnl_repca/commit/363e3b9bc982df609dcbddaab00b03ddfa1ac07d))

## 0.0.149 (2023-08-18)


### Bug Fixes

* auto publish ([4520bfc](https://gitlab.com/rndDesto/sgnl_repca/commit/4520bfcca6e9dfef62d47543b65551ad444402c1))

## 0.0.148 (2023-08-18)


### Bug Fixes

* coba publish ([25f8a8f](https://gitlab.com/rndDesto/sgnl_repca/commit/25f8a8f6da7c461f05c25a82c0c26fd2816efb75))

## 0.0.147 (2023-08-18)


### Bug Fixes

* tailwind config ([7dae6ee](https://cicd-scm.telkomsel.co.id/signal/signal-ui/commit/7dae6eee5120f216d82da6763e3de3ca24ec7c9b))

## 0.0.146 (2023-08-16)


### Bug Fixes

* compoenetn paper ([1675a42](https://cicd-scm.telkomsel.co.id/signal/signal-ui/commit/1675a421d29105d244f1715002c34471501162d0))

## 0.0.145 (2023-08-16)


### Bug Fixes

* update storybook ([5004ae5](https://cicd-scm.telkomsel.co.id/signal/signal-ui/commit/5004ae566df3374464fb0c721a78a448e87784fa))

## 0.0.144 (2023-08-15)


### Bug Fixes

* fixing heading react ([d23bbdd](https://cicd-scm.telkomsel.co.id/signal/signal-ui/commit/d23bbdd3d6a1b1f3c8e111fff75fb37b25a14e18))
* fixing import signal heading ([237e9e6](https://cicd-scm.telkomsel.co.id/signal/signal-ui/commit/237e9e6a9f0aa9179391458c286b1181a260c1b2))
* fixing index typograpy import ([2a4ff9e](https://cicd-scm.telkomsel.co.id/signal/signal-ui/commit/2a4ff9e055fb39500556a82797ee2f40519b7365))

## 0.0.143 (2023-08-15)


### Bug Fixes

* enhance typo ([1ac8dfc](https://cicd-scm.telkomsel.co.id/signal/signal-ui/commit/1ac8dfc01ebd58f48f5c0bd9115e43407b32526f))

## 0.0.142 (2023-08-15)


### Bug Fixes

* signal typography ([ed96aec](https://cicd-scm.telkomsel.co.id/signal/signal-ui/commit/ed96aec8f616e42d0cfc3d14bbe2f3e9c5dac2c1))

## 0.0.141 (2023-08-15)


### Bug Fixes

* refactor_import_index ([e1378c5](https://cicd-scm.telkomsel.co.id/signal/signal-ui/commit/e1378c5b001017280e504a1548d7e8a0e658f93a))

## 0.0.140 (2023-08-09)


### Bug Fixes

* del spaces ([60aa1cf](https://cicd-scm.telkomsel.co.id/signal/signal-ui/commit/60aa1cf24756a184561839f4c6eb556ac028530c))
* skip push ([5bb4175](https://cicd-scm.telkomsel.co.id/signal/signal-ui/commit/5bb4175968d33b55ce8d566a06ea2cafcc0bf595))

## 0.0.139 (2023-08-09)


### Bug Fixes

* coba rilis ke jfrog ([3e337f7](https://cicd-scm.telkomsel.co.id/signal/signal-ui/commit/3e337f7047edfde6b1b5e12c487d9e3af42d52e4))

## 0.0.138 (2023-08-08)


### Bug Fixes

* republish paket ([d26c0c3](https://cicd-scm.telkomsel.co.id/signal/signal-ui/commit/d26c0c3432013a84ce2e79afc406e9968a56c78c))
* update versi ([2b1e6b5](https://cicd-scm.telkomsel.co.id/signal/signal-ui/commit/2b1e6b5f4348bcc76d9a97ed9effe33ceb17acb5))

## 0.0.137 (2023-08-08)


### Bug Fixes

* adjust sturktur button ([38576c1](https://cicd-scm.telkomsel.co.id/signal/signal-ui/commit/38576c1549426dcac2a134d210529c0c3db6e71b))
* modif props button ([442791b](https://cicd-scm.telkomsel.co.id/signal/signal-ui/commit/442791b7ef2ce1793005bedb00ec6419a37aa3e6))
* update storybook button ([a8edbe3](https://cicd-scm.telkomsel.co.id/signal/signal-ui/commit/a8edbe30baf35b8298e01bb626b9e95193106fe8))

## 0.0.136 (2023-08-03)


### Bug Fixes

* rm skrip hook ([90dc7cb](https://cicd-scm.telkomsel.co.id/signal/signal-ui/commit/90dc7cb89f96d0acede0c9a80ba5e2c6c07b6d08))

## 0.0.135 (2023-08-03)


### Bug Fixes

* adjust paket url ([2a4d1e3](https://cicd-scm.telkomsel.co.id/signal/signal-ui/commit/2a4d1e3a33d5bcafc4ff4861a1e4e560a8c12caa))
* benerin link ([b3addb6](https://cicd-scm.telkomsel.co.id/signal/signal-ui/commit/b3addb6f7108108be32c79c85d044f5d9f977588))
* del yml n hook ([76092e7](https://cicd-scm.telkomsel.co.id/signal/signal-ui/commit/76092e7a3631c31d69c62ad0bb6992a5a2c20489))
* revert paket config ([5ada2c4](https://cicd-scm.telkomsel.co.id/signal/signal-ui/commit/5ada2c47d6a0d29e82484abfb9e5a93940ef7ae0))
* tambah cz ([53a06af](https://cicd-scm.telkomsel.co.id/signal/signal-ui/commit/53a06afc4855d7d243dd002c9900f8b6562aca1f))
* tambah script dry run ([24c2f0f](https://cicd-scm.telkomsel.co.id/signal/signal-ui/commit/24c2f0f87bf46708fde699c085148bf32a113416))