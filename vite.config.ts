import { resolve } from 'path'
import { defineConfig, splitVendorChunkPlugin } from 'vite'

// https://vitejs.dev/config/
export default defineConfig({
	resolve: {
		alias: [
		  {
			find: "@",
			replacement: resolve(__dirname, "./src"),
		  },
		],
	  },
	plugins: [splitVendorChunkPlugin()],
	build: {
	  lib: {
		entry: resolve(__dirname, 'src/signal-ui.ts'),
		name: 'SignalUI',
		fileName: 'signal-ui'
	  },
	  rollupOptions: {
		external: ['react'],
		output: {
			globals: {
				react: 'React',
			},
		},
	  }
	}
})
  