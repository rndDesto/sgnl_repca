import '../src/styles/typography.css';
import '../src/styles/color.scss';
import '../src/styles/shadow.scss';
import '../src/styles/spacing.scss';

export const parameters = {
  actions: { argTypesRegex: "^on[A-Z].*" },
  controls: {
    matchers: {
      color: /(background|color)$/i,
      date: /Date$/,
    },
  },
}