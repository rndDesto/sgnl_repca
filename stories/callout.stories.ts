import { Meta, StoryObj } from '@storybook/web-components'
import { html } from 'lit'
import { withActions } from '@storybook/addon-actions/decorator'
import '../src/components'

export default {
  title: 'Overlay/Callout',
  parameters: {
    actions: {
      handles: ['onClickAction', 'onClose'],
    },
    design: {
      type: 'figma',
      url: 'https://www.figma.com/file/tI9cLemXH9Z2mwYQb0Pn47/Signal---Web-Kit?type=design&node-id=481-165714&mode=design&t=4FQKB1iGtZatd8Fz-0',
    },
    docs: {
      description: {
        component: 'A callout is a type of text box or graphic element used to highlight or draw attention to a specific piece of content within a larger design or layout. Callouts can be used to provide additional information or context, or to emphasize a particular point or message.',
      },
      story: { autoplay: true },
    },
  },
  decorators: [withActions],
  argTypes: {
    class: {
      description: 'The `class` attribute is often used to point to a class name in a style sheet. It can also be used by a JavaScript to access and manipulate elements with the specific class name',
      type: 'string'
    },
    titleMessage: {
      description: 'Callout title message. It should be string',
      type: 'string'
    },
    message: {
      description: 'Callout message. It should be string',
      type: 'string'
    },
    textButton: {
      description: 'Callout text button. It should be string',
      type: 'string'
    },
    type: {
      description: 'Callout type. One of type: `info`, `warning`',
      options: ['info', 'warning'],
      control: { type: 'radio' }
    },
    closeable: {
      description: 'When callout wants to be closeable, it should be set as `true`',
      type: 'boolean'
    }
  },
  render: (args) => html`<signal-callout titleMessage=${args.titleMessage} message=${args.message} textButton=${args.textButton} type=${args.type} ?closeable=${args.closeable}/>`,
} as Meta

export const Default: StoryObj = {
  name: 'Default',
  args: {
    titleMessage: 'Kamu bisa lakukan ini',
    message: 'Periksa ejaan kata kunci yang kamu ketik',
    textButton: 'Action',
    type: 'info',
    closeable: false,
  },
}