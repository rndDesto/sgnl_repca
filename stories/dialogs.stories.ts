import { Meta, StoryObj } from '@storybook/web-components'
import { html } from 'lit'
import { withActions } from '@storybook/addon-actions/decorator';
import '../src/components'

export default {
  title: 'Overlay/Dialogs',
  parameters: {
    actions: {
      handles: ['onClickSecondaryButton', 'onClickPrimaryButton'],
    },
    design: {
      type: 'figma',
      url: 'https://www.figma.com/file/tI9cLemXH9Z2mwYQb0Pn47/Signal---Web-Kit?type=design&node-id=3831-301960&mode=design&t=4FQKB1iGtZatd8Fz-0',
    },
    docs: {
      description: {
        component: 'Elements that are used to display temporary or modal content, typically in the form of a pop-up window or overlay. Dialogs are commonly used to present important information, prompt for user input, or provide additional context or actions without disrupting the main flow of the application or webpage.',
      },
      story: { autoplay: true },
    }
  },
  decorators: [withActions],
  argTypes: {
    class: {
      description: 'The `class` attribute is often used to point to a class name in a style sheet. It can also be used by a JavaScript to access and manipulate elements with the specific class name',
      type: 'string'
    },
    slot: {
      description: 'Dialogs slot. It can be put using html tag',
      type: 'function',
    }
  },
  render: (args) => html`<signal-dialogs>
  <p>
  Kami menggunakan cookie untuk meningkatkan pengalaman Anda menelusuri situs kami. Silahkan menyetujui 
  <a style="color:#0050AE; text-decoration:none; font-weight:700" href="https://www.telkomsel.com/terms-and-conditions">Syarat & Ketentuan</a>
  dan 
  <a style="color:#0050AE; text-decoration:none; font-weight:700" href="https://www.telkomsel.com/privacy-policy">Kebijakan Privasi</a>
  untuk pengalaman lebih baik. 
  </p>
  </signal-dialogs>`,
} as Meta

export const Default: StoryObj = {
  name: 'Default',
}