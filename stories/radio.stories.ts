import { Meta, StoryObj } from '@storybook/web-components'
import { html } from 'lit'
import { withActions } from '@storybook/addon-actions/decorator'
import '../src/components'

export default {
  title: 'Selection Control/Radio Button',
  parameters: {
    actions: {
      handles: ['onChange'],
    },
    design: {
      type: 'figma',
      url: 'https://www.figma.com/file/tI9cLemXH9Z2mwYQb0Pn47/Signal---Web-Kit?type=design&node-id=4406-332588&mode=design&t=4FQKB1iGtZatd8Fz-0',
  },
    docs: {
      description: {
        component: 'Element that allows the user to select a single option from a set of mutually exclusive choices. It consists of a small circle or dot that can be filled in or left empty, depending on whether the user wants to select the option or not. When a radio button is selected, it is typically indicated by a filled-in circle or dot.',
      },
      story: { autoplay: true },
    },
  },
  decorators: [withActions],
  argTypes: {
    class: {
      description: 'The `class` attribute is often used to point to a class name in a style sheet. It can also be used by a JavaScript to access and manipulate elements with the specific class name',
      type: 'string'
    },
    label: {
      description: 'Radio label. It should be string',
      type: 'string'
    },
    selected: {
      defaultValue: { summary: false },
      description: 'Selected radio. If selected is set to `true`, radio will be filled by blue background color',
      type: 'boolean'
    },
    disabled: {
      defaultValue: { summary: false },
      description: "Disabled radio. If disabled is set to `true`, radio can't be clicked",
      type: 'boolean'
    },
    labelPosition: {
      control: { type: 'radio' },
      defaultValue: { summary: 'right' },
      description: "Radio label position. One of type: `left`, `right`",
      options: ['left', 'right'],
    },
    value: {
      description: "Radio value. When radio is clicked, the callback will return its value. It should be string",
      type: 'string'
    },
  },
  render: (args) => html`<signal-radio ?selected=${args.selected} ?disabled=${args.disabled} labelPosition=${args.labelPosition} value=${args.value}>${args.label}</signal-radio>`,
} as Meta

export const Default: StoryObj = {
  name: 'Default',
  args: {
    label: 'Hey, this is radio label',
    selected: false,
    disabled: false,
    labelPosition: 'right'
  },
}