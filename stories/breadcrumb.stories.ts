import { Meta, StoryObj } from '@storybook/web-components'
import { html } from 'lit'
import '../src/components'

export default {
  title: 'Navigation/Breadcrumbs',
  parameters: {
    design: {
      type: 'figma',
      url: 'https://www.figma.com/file/tI9cLemXH9Z2mwYQb0Pn47/Signal---Web-Kit?type=design&node-id=1720-416821&mode=design&t=4FQKB1iGtZatd8Fz-0',
    },
    docs: {
      description: {
        component: 'In the context of web design and navigation, breadcrumbs refer to a navigational aid that helps users understand their current location within a website and provides a way for them to easily navigate back to higher-level pages.',
      },
      story: { autoplay: true },
    },
  },
  argTypes: {
    class: {
      description: 'The `class` attribute is often used to point to a class name in a style sheet. It can also be used by a JavaScript to access and manipulate elements with the specific class name',
      type: 'string'
    },
    crumbs: {
      description: 'Crumb children inside breadcrumbs. It should be defined as `title` object and `route` object of array, eg: `[{"title": "Home", "route": "/"}]`. When the children length is more than 5 row, it will automatically hide the middle child'
    },
  },
  render: (args) => html`<signal-breadcrumb crumbs=${args.crumbs}/>`,
} as Meta

export const Default: StoryObj = {
  name: 'Default',
  args: {
    crumbs: '[{"title": "Home", "route": "/"}, {"title": "Category", "route": "/category"},  {"title": "Portfolio", "route": "/portfolio"}, {"title": "Profile", "route": "/profile"}, {"title": "Education", "route": "/education"}, {"title": "Address", "route": "/address"}]',
  },
}