import { Meta, StoryObj } from '@storybook/web-components'
import { html } from 'lit'
import { withActions } from '@storybook/addon-actions/decorator'
import '../src/components'

export default {
  title: 'Selection Control/Language Selector',
  parameters: {
    actions: {
      handles: ['onChange'],
    },
    design: {
      type: 'figma',
      url: 'https://www.figma.com/file/tI9cLemXH9Z2mwYQb0Pn47/Signal---Web-Kit?type=design&node-id=1597-417948&mode=design&t=4FQKB1iGtZatd8Fz-0',
    },
    docs: {
      description: {
        component: 'Element that allows users to choose the language in which they want to view the content of a software application, website, or other digital interface.',
      },
      story: { autoplay: true },
    },
  },
  decorators: [withActions],
  argTypes: {
    class: {
      description: 'The `class` attribute is often used to point to a class name in a style sheet. It can also be used by a JavaScript to access and manipulate elements with the specific class name',
      type: 'string'
    },
    selectedLanguage: {
      control: { type: 'radio' },
      defaultValue: { summary: 'id' },
      description: 'Selected language selector. One of type: `id`, `en`',
      options: ['id', 'en'],
    },
    chevron: {
      defaultValue: { summary: false },
      description: 'Language selector chevron. If it is enabled, chevron icon will be shown next to label',
      type: 'boolean'
    }
  },
  render: (args) => html`<signal-language-selector selectedLanguage=${args.selectedLanguage} ?chevron=${args.chevron}></signal-language-selector>`,
} as Meta

export const Default: StoryObj = {
  name: 'Default',
  args: {
    selectedLanguage: 'id',
    chevron: false
  },
}