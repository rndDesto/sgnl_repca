import { Meta, StoryObj } from '@storybook/web-components'
import { html } from 'lit'
import { withActions } from '@storybook/addon-actions/decorator'
import '../src/components'

export default {
  title: 'Banner/Banner Image',
  parameters: {
    actions: {
      handles: ['onClick'],
    },
    design: {
      type: 'figma',
      url: 'https://www.figma.com/file/tI9cLemXH9Z2mwYQb0Pn47/Signal---Web-Kit?type=design&node-id=888-239680&mode=design&t=4FQKB1iGtZatd8Fz-0',
    },
    docs: {
      description: {
        component: 'The banner may also include pre-designed templates that can be customized with specific messaging and imagery to suit different campaigns or promotions. These templates may be available in various sizes and formats, depending on the intended platform and placement of the banner.',
      },
      story: { autoplay: true },
    },
  },
  decorators: [withActions],
  argTypes: {
    class: {
      description: 'The `class` attribute is often used to point to a class name in a style sheet. It can also be used by a JavaScript to access and manipulate elements with the specific class name',
      type: 'string'
    },
    image: {
      description: "Banner image. It should be string, can be url or local path. eg: `https://www.edigitalagency.com.au/wp-content/uploads/new-Instagram-logo-white-glyph.png` or `/assets/icon.png`",
      type: "string"
    },
    variant: {
      control: { type: 'radio' },
      defaultValue: { summary: 'brand-sm' },
      description: 'Banner variant. One of type: `brand-sm`, `brand-lg`, `rectangle-sm`, `rectangle-md`, `rectangle-lg`, `square-sm`, `square-lg`',
      options: ['brand-sm', 'brand-lg', 'rectangle-sm', 'rectangle-md', 'rectangle-lg', 'square-sm', 'square-lg'],
    },
    width: {
      description: 'Banner width',
      type: "number"
    },
    height: {
      description: 'Banner height',
      type: "number"
    }
  },
  render: (args) => html`<signal-banner variant=${args.variant} image=${args.image} width=${args.width} height=${args.height}/>`,
} as Meta

export const BrandSm: StoryObj = {
  name: 'brand-sm',
  args: {
    image: 'https://www.telkomsel.com/sites/default/files/box_media/left/desktop/Desk-LP-MenangByk-800x450-min.jpg',
    variant: 'brand-sm',
    width: '',
    height: ''
  },
}

export const BrandLg: StoryObj = {
  name: 'brand-lg',
  args: {
    image: 'https://www.telkomsel.com/sites/default/files/box_media/left/desktop/Desk-LP-MenangByk-800x450-min.jpg',
    variant: 'brand-lg',
    width: '',
    height: ''
  },
}

export const RectangleSm: StoryObj = {
  name: 'rectangle-sm',
  args: {
    image: 'https://www.telkomsel.com/sites/default/files/box_media/left/desktop/Desk-LP-MenangByk-800x450-min.jpg',
    variant: 'rectangle-sm',
    width: '',
    height: ''
  },
}

export const RectangleMd: StoryObj = {
  name: 'rectangle-md',
  args: {
    image: 'https://www.telkomsel.com/sites/default/files/box_media/left/desktop/Desk-LP-MenangByk-800x450-min.jpg',
    variant: 'rectangle-md',
    width: '',
    height: ''
  },
}

export const RectangleLg: StoryObj = {
  name: 'rectangle-lg',
  args: {
    image: 'https://www.telkomsel.com/sites/default/files/box_media/left/desktop/Desk-LP-MenangByk-800x450-min.jpg',
    variant: 'rectangle-lg',
    width: '',
    height: ''
  },
}

export const SquareSm: StoryObj = {
  name: 'square-sm',
  args: {
    image: 'https://www.telkomsel.com/sites/default/files/box_media/left/desktop/Desk-LP-MenangByk-800x450-min.jpg',
    variant: 'square-sm',
    width: '',
    height: ''
  },
}

export const SquareLg: StoryObj = {
  name: 'square-lg',
  args: {
    image: 'https://www.telkomsel.com/sites/default/files/box_media/left/desktop/Desk-LP-MenangByk-800x450-min.jpg',
    variant: 'square-lg',
    width: '',
    height: ''
  },
}