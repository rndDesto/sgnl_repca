import { Meta, StoryObj } from '@storybook/web-components'
import { html } from 'lit'
import { withActions } from '@storybook/addon-actions/decorator'
import '../src/components'

export default {
  title: 'Selection Control/Chips',
  parameters: {
    actions: {
      handles: ['onClick'],
    },
    design: {
      type: 'figma',
      url: 'https://www.figma.com/file/tI9cLemXH9Z2mwYQb0Pn47/Signal---Web-Kit?type=design&node-id=368-140414&mode=design&t=4FQKB1iGtZatd8Fz-0',
    },
    docs: {
      description: {
        component: 'In the context of design systems, "chips" refer to small visual elements or components that display succinct information or represent selected options.'
      }
    },
    story: { autoplay: true },
  },
  decorators: [withActions],
  argTypes: {
    class: {
      description: 'The `class` attribute is often used to point to a class name in a style sheet. It can also be used by a JavaScript to access and manipulate elements with the specific class name',
      type: 'string'
    },
    text: {
      description: 'Chips text. It should be string',
      type: 'string',
    },
    disabled: {
      defaultValue: { summary: false },
      description: "Disabled chips. If button is disabled, it can't be clicked",
      type: 'boolean',
    },
    leftIcon: {
      description: "Button left icon. It should be string, can be url or local path. eg: `https://www.edigitalagency.com.au/wp-content/uploads/new-Instagram-logo-white-glyph.png` or `/assets/icon.png`",
      type: 'string',
    },
    rightIcon: {
      description: "Button right icon. It should be string, can be url or local path. eg: `https://www.edigitalagency.com.au/wp-content/uploads/new-Instagram-logo-white-glyph.png` or `/assets/icon.png`",
      type: 'string',
    },
    selected: {
      defaultValue: { summary: false },
      description: 'Selected chips. If chips is selected, the background will have color',
      type: 'boolean'
    },
  },
  render: (args) => html`<signal-chips ?disabled=${args.disabled} ?selected=${args.selected} leftIcon=${args.leftIcon} rightIcon=${args.rightIcon}>${args.text}</signal-chips>`,
} as Meta

export const Default: StoryObj = {
  name: 'Default',
  args: {
    text: 'Category',
    disabled: false,
    selected: false,
    leftIcon: '',
    rightIcon: '',
  },
}