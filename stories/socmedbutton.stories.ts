import { Meta, StoryObj } from '@storybook/web-components'
import { html } from 'lit'
import { withActions } from '@storybook/addon-actions/decorator'
import '../src/components'

export default {
  title: 'Buttons/Social Media Button',
  parameters: {
    actions: {
      handles: ['onClick'],
    },
    design: {
      type: 'figma',
      url: 'https://www.figma.com/file/tI9cLemXH9Z2mwYQb0Pn47/Signal---Web-Kit?type=design&node-id=322-94555&mode=design&t=4FQKB1iGtZatd8Fz-0',
    },
    docs: {
      description: {
        component: 'A social media button is a graphical element on a website or app that allows users to connect with or share content on various social media platforms. These buttons typically include the logo or icon of the social media platform.',
      },
      story: { autoplay: true },
    },
  },
  decorators: [withActions],
  argTypes: {
    class: {
      description: 'The `class` attribute is often used to point to a class name in a style sheet. It can also be used by a JavaScript to access and manipulate elements with the specific class name',
      type: 'string'
    },
    variant: {
      control: { type: 'radio' },
      defaultValue: { summary: 'apple' },
      description: 'Social media button variant. One of type: `apple`, `facebook`, `google`, `twitter`',
      options: ['apple', 'facebook', 'google', 'twitter'],
    },
  },
  render: (args) => html`<signal-button-socmed variant=${args.variant}></signal-button-socmed>`,
} as Meta

export const Default: StoryObj = {
  name: 'Default',
  args: {
    variant: 'apple',
  },
}