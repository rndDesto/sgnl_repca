import { Meta, StoryObj } from '@storybook/web-components'
import { html } from 'lit'
import '../src/components'

export default {
    title: 'Overlay/Snackbar',
    parameters: {
        design: {
            type: 'figma',
            url: 'https://www.figma.com/file/tI9cLemXH9Z2mwYQb0Pn47/Signal---Web-Kit?type=design&node-id=1271-389590&mode=design&t=4FQKB1iGtZatd8Fz-0',
        },
        docs: {
            description: {
                component: 'Snackbars provide brief messages about app processes at the bottom of the screen.',
            },
            story: { autoplay: true },
        },
    },
    argTypes: {
        class: {
            description: 'The `class` attribute is often used to point to a class name in a style sheet. It can also be used by a JavaScript to access and manipulate elements with the specific class name',
            type: 'string'
        },
        text: {
            description: 'Snackbar text. It should be string and it has maximum 2 lines',
            type: 'string'
        },
        variant: {
            control: { type: 'radio' },
            defaultValue: { summary: 'general' },
            description: 'Snackbar variant. One of type: `general`, `warning`',
            options: ['general', 'warning'],
        },
        textButton: {
            description: 'Snackbar text button. It should be string',
            type: 'string'
        },
        duration: {
            control: { type: 'radio' },
            defaultValue: { summary: 3000 },
            description: 'Snackbar duration. One of type: `3000`, `5000`',
            options: [3000, 5000],
        },
        show: {
            description: 'Show snackbar. If it is set as `true`, snackbar will be shown as long as the duration set',
            defaultValue: { summary: false },
            type: 'boolean'
        },
    },
    render: (args) => html`<signal-snackbar variant=${args.variant} textButton=${args.textButton} duration=${args.duration} .show=${args.show}>${args.text}</signal-snackbar>`,
} as Meta

export const Default: StoryObj = {
    name: 'Default',
    args: {
        text: "Can't send photo. Retry in 5 seconds",
        variant: 'general',
        textButton: 'undo',
        duration: 3000,
        show: false
    },
}