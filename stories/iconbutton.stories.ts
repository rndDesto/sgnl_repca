import { Meta, StoryObj } from '@storybook/web-components'
import { html } from 'lit'
import { withActions } from '@storybook/addon-actions/decorator'
import '../src/components'

export default {
  title: 'Buttons/Icon Button',
  parameters: {
    actions: {
      handles: ['onClick'],
    },
    design: {
      type: 'figma',
      url: 'https://www.figma.com/file/tI9cLemXH9Z2mwYQb0Pn47/Signal---Web-Kit?type=design&node-id=326-96159&mode=design&t=4FQKB1iGtZatd8Fz-0',
    },
    docs: {
      description: {
        component: 'An icon button is a graphical element on a website, app, or digital document that is designed to prompt a user to take a specific action.',
      },
      story: { autoplay: true },
    },
  },
  decorators: [withActions],
  argTypes: {
    class: {
      description: 'The `class` attribute is often used to point to a class name in a style sheet. It can also be used by a JavaScript to access and manipulate elements with the specific class name',
      type: 'string'
    },
    icon: {
      description: "Icon button icon. It should be string, can be url or local path. eg: `https://www.edigitalagency.com.au/wp-content/uploads/new-Instagram-logo-white-glyph.png` or `/assets/icon.png`",
      type: 'string',
    },
    variant: {
      control: { type: 'radio' },
      defaultValue: { summary: 'primary' },
      description: 'Icon button variant. One of type: `primary`, `secondary`',
      options: ['primary', 'secondary'],
    },
    disabled: {
      defaultValue: { summary: false },
      description: "Disabled icon button. If icon button is disabled, it can't be clicked",
      type: 'boolean',
    },
  },
  render: (args) => html`<signal-button-icon variant=${args.variant} icon=${args.icon} ?disabled=${args.disabled}></signal-button-icon>`,
} as Meta

export const Default: StoryObj = {
  name: 'Default',
  args: {
    icon: 'https://www.pngall.com/wp-content/uploads/5/Red-Shopping-Cart-PNG-Picture.png',
    variant: 'primary',
    disabled: false,
  },
}