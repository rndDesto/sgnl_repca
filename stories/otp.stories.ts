import { Meta, StoryObj } from '@storybook/web-components'
import { html } from 'lit'
import { withActions } from '@storybook/addon-actions/decorator'
import '../src/components'

export default {
  title: 'Input/OTP',
  parameters: {
    actions: {
      handles: ['onChange'],
    },
    design: {
      type: 'figma',
      url: 'https://www.figma.com/file/tI9cLemXH9Z2mwYQb0Pn47/Signal---Web-Kit?type=design&node-id=1725-417242&mode=design&t=4FQKB1iGtZatd8Fz-0',
    },
    docs: {
      description: {
        component: 'OTP is typically sent to the user via SMS, email, or a dedicated authentication app. Once entered correctly, the OTP is verified and the user is granted access to the requested service. Considered a more secure form of authentication than traditional passwords, which can be easily guessed or stolen.',
      },
      story: { autoplay: true },
    },
  },
  decorators: [withActions],
  argTypes: {
    class: {
      description: 'The `class` attribute is often used to point to a class name in a style sheet. It can also be used by a JavaScript to access and manipulate elements with the specific class name',
      type: 'string'
    },
    digit: {
      control: { type: 'radio' },
      defaultValue: { summary: 6 },
      description: 'OTP digit. One of type: `4`, `5`, `6`',
      options: [4, 5, 6],
      type: 'number'
    },
    isError: {
      defaultValue: { summary: false },
      description: 'OTP error. If it is set as true, otp ui error will show',
      type: 'boolean'
    },
    disabled: {
      defaultValue: { summary: false },
      description: "Disabled OTP. If OTP is disabled, it can't be clicked",
      type: 'boolean',
    },
  },
  render: (args) => html`<signal-otp digit=${args.digit} ?disabled=${args.disabled} ?isError=${args.isError}></signal-otp>`,
} as Meta

export const Default: StoryObj = {
  name: 'Default',
  args: {
    digit: 6,
    isError: false,
    disabled: false,
  },
}