import { Meta, StoryObj } from '@storybook/web-components'
import { html } from 'lit'
import '../src/components'

export default {
  title: 'Navigation/Tab',
  parameters: {
    design: {
      type: 'figma',
      url: 'https://www.figma.com/file/tI9cLemXH9Z2mwYQb0Pn47/Signal---Web-Kit?type=design&node-id=1185-331138&mode=design&t=4FQKB1iGtZatd8Fz-0',
    },
    docs: {
      description: {
        component: 'A tab bar is a graphical user interface (GUI) element that appears at the bottom of an application or website and typically contains a row of clickable tabs or buttons.',
      },
      story: { autoplay: true },
    },
  },
  argTypes: {
    class: {
      description: 'The `class` attribute is often used to point to a class name in a style sheet. It can also be used by a JavaScript to access and manipulate elements with the specific class name',
      type: 'string'
    },
    label: {
      description: 'Tab label. It should be array of string, eg: `["Internet", "Roaming", "Entertainment"]`',
    },
    selected: {
      description: 'Selected tab. It should be array of boolean, eg: `[true, false, false]`. If tab child is set to `true`, the active tab will be shown'
    },
    isCustomIcon: {
      description: 'Custom icon tab. If it is set to `true`, prop icon should be define too in order to show the icon',
      type: 'boolean'
    },
    icon: {
      description: "Tab icon. It should be string, can be url or local path. eg: `https://www.edigitalagency.com.au/wp-content/uploads/new-Instagram-logo-white-glyph.png` or `/assets/icon.png`",
      type: "string",
    },
  },
  render: (args) => html`
    <signal-tabs>
      <signal-tab label=${args.label[0]} ?selected=${args.selected[0]} ?isCustomIcon=${args.isCustomIcon} .icon=${args.icon}></signal-tab>
      <signal-tab label=${args.label[1]} ?selected=${args.selected[1]} ?isCustomIcon=${args.isCustomIcon} .icon=${args.icon}></signal-tab>
      <signal-tab label=${args.label[2]} ?selected=${args.selected[2]} ?isCustomIcon=${args.isCustomIcon} .icon=${args.icon}></signal-tab>
      <signal-tab label=${args.label[3]} ?selected=${args.selected[3]} ?isCustomIcon=${args.isCustomIcon} .icon=${args.icon}></signal-tab>
      <signal-tab label=${args.label[4]} ?selected=${args.selected[4]} ?isCustomIcon=${args.isCustomIcon} .icon=${args.icon}></signal-tab>
      <signal-tab label=${args.label[5]} ?selected=${args.selected[5]} ?isCustomIcon=${args.isCustomIcon} .icon=${args.icon}></signal-tab>
      <signal-tab label=${args.label[6]} ?selected=${args.selected[6]} ?isCustomIcon=${args.isCustomIcon} .icon=${args.icon}></signal-tab>
      <signal-tab label=${args.label[7]} ?selected=${args.selected[7]} ?isCustomIcon=${args.isCustomIcon} .icon=${args.icon}></signal-tab>
      <signal-tab label=${args.label[8]} ?selected=${args.selected[8]} ?isCustomIcon=${args.isCustomIcon} .icon=${args.icon}></signal-tab>
      </signal-tabs>
    `,
} as Meta

export const Default: StoryObj = {
  name: 'Default',
  args: {
    label: ['Internet sgsgsfg sfsfhs sfhsfh sfhsfhsfh sfhsfh ', 'Roaming', 'Roaming', 'Entertainment', 'Voice & SMS', 'Voice & SMS', 'Voice & SMS', 'Voice & SMS', 'Voice & SMS'],
    isCustomIcon: false,
    selected: [true, false, false, false, false, false, false, false, false, false],
    icon: 'https://signal-design-system.web.app/_next/static/media/ReplaceSIMCard.6b4fce6a.png'
  },
}