import { Meta, StoryObj } from '@storybook/web-components'
import { html } from 'lit'
import { withActions } from '@storybook/addon-actions/decorator'
import '../src/components'

export default {
    title: 'Input/Text Field',
    parameters: {
        actions: {
            handles: ['onChange', 'onClickIconInfo', 'onError', 'onSubmit'],
        },
        design: {
            type: 'figma',
            url: 'https://www.figma.com/file/tI9cLemXH9Z2mwYQb0Pn47/Signal---Web-Kit?type=design&node-id=1725-417185&mode=design&t=4FQKB1iGtZatd8Fz-0',
        },
        docs: {
            description: {
                component: 'A text field is an input area on a form or web page where a user can type in text. It typically consists of a rectangular box with a blinking cursor indicating where text can be entered.',
            },
            story: { autoplay: true },
        },
    },
    decorators: [withActions],
    argTypes: {
        class: {
            description: 'The `class` attribute is often used to point to a class name in a style sheet. It can also be used by a JavaScript to access and manipulate elements with the specific class name',
            type: 'string'
        },
        label: {
            description: 'Text field label. It should be string',
            type: 'string'
        },
        isError: {
            defaultValue: { summary: false },
            description: 'Error text field. If it is set as true, error state will be shown',
            type: 'boolean'
        },
        isValid: {
            defaultValue: { summary: false },
            description: 'Valid text field. If it is set as true, valid state will be shown',
            type: 'boolean'
        },
        isLoading: {
            defaultValue: { summary: false },
            description: 'Loading text field. If it is set as `true`, loading state will be shown',
            type: 'boolean'
        },
        disabled: {
            defaultValue: { summary: false },
            description: "Disabled text field. If text area is disabled, it can't be clicked",
            type: 'boolean'
        },
        showInfo: {
            defaultValue: { summary: false },
            description: 'Show info in text field. If it is set as `true`, info icon will be shown',
            type: 'boolean'
        },
        required: {
            defaultValue: { summary: false },
            description: 'Required text field. If it is set as `true`, mandatory status will be shown',
            type: 'boolean'
        },
        placeholder: {
            description: 'Placeholder text area. It should be string',
            type: 'string'
        },
        obscureText: {
            defaultValue: { summary: false },
            description: 'Show eye for password. If it is set as `true`, eye icon will be shown to hide/show the password text and any type of textfield will be set as `text` input',
            type: 'boolean'
        },
        type: {
            control: { type: 'radio' },
            defaultValue: { summary: 'text' },
            description: 'Text field type. One of type: `text`, `password`, `email`, `number`',
            options: ['text', 'password', 'email', 'number'],
        },
        maxLength: {
            defaultValue: { summary: 200 },
            description: 'Max length character in text field. It should be number',
            type: 'number'
        },
        minLength: {
            defaultValue: { summary: 0 },
            description: 'Min length character in text field. It should be number',
            type: 'number'
        },
    },
    render: (args) => html`<signal-textfield label=${args.label} ?isError=${args.isError} ?isValid=${args.isValid} ?isLoading=${args.isLoading} ?disabled=${args.disabled} ?showInfo=${args.showInfo} ?required=${args.required} placeholder=${args.placeholder} ?obscureText=${args.obscureText} type=${args.type} maxLength=${args.maxLength} minLength=${args.maxLength}></signal-textfield>`,
} as Meta

export const Default: StoryObj = {
    name: 'Default',
    args: {
        label: 'Username',
        isError: false,
        isValid: false,
        isLoading: false,
        disabled: false,
        showInfo: false,
        required: true,
        placeholder: 'Masukkan username anda',
        obscureText: false,
        type: 'text',
        maxLength: 200,
        minLength: 0
    },
}