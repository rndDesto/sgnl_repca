import { Meta, StoryObj } from '@storybook/web-components'
import { html } from 'lit'
import '../src/components'

export default {
  title: 'Selection Control/Badges',
  parameters: {
    design: {
      type: 'figma',
      url: 'https://www.figma.com/file/tI9cLemXH9Z2mwYQb0Pn47/Signal---Web-Kit?type=design&node-id=1597-417930&mode=design&t=4FQKB1iGtZatd8Fz-0',
    },
    docs: {
      description: {
        component: 'Element that displays a small amount of information in a visually distinctive way. It usually consists of a small shape, such as a circle or rectangle, that contains a number or text',
      },
      story: { autoplay: true },
    },
  },
  argTypes: {
    class: {
      description: 'The `class` attribute is often used to point to a class name in a style sheet. It can also be used by a JavaScript to access and manipulate elements with the specific class name',
      type: 'string'
    },
    value: {
      description: 'Badge value. It must be string, eg: 99+',
      type: "string"
    },
    variant: {
      control: { type: 'radio' },
      defaultValue: { summary: 'primary' },
      description: 'Badge variant. One of type: `primary`, `secondary`',
      options: ['primary', 'secondary'],
    },
  },
  render: (args) => html`<signal-badge variant=${args.variant}>${args.value}</signal-badge>`,
} as Meta

export const Primary: StoryObj = {
  name: 'Primary',
  args: {
    value: '99+',
    variant: 'primary'
  },
}

export const Secondary: StoryObj = {
  name: 'Secondary',
  args: {
    value: '99+',
    variant: 'secondary'
  },
}