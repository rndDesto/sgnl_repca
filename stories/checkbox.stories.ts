import { Meta, StoryObj } from '@storybook/web-components'
import { html } from 'lit'
import { withActions } from '@storybook/addon-actions/decorator'
import '../src/components'

export default {
  title: 'Selection Control/Checkbox',
  parameters: {
    actions: {
      handles: ['onChange'],
    },
    design: {
      type: 'figma',
      url: 'https://www.figma.com/file/tI9cLemXH9Z2mwYQb0Pn47/Signal---Web-Kit?type=design&node-id=4406-332508&mode=design&t=4FQKB1iGtZatd8Fz-0',
    },
    docs: {
      description: {
        component: 'Element that allows the user to make a binary selection from one or more options. It consists of a small square box that can be checked or unchecked, depending on whether the user wants to select the option or not. When a checkbox is checked, it is typically indicated by a small tick or checkmark inside the box.',
      },
      story: { autoplay: true },
    }
  },
  decorators: [withActions],
  argTypes: {
    class: {
      description: 'The `class` attribute is often used to point to a class name in a style sheet. It can also be used by a JavaScript to access and manipulate elements with the specific class name',
      type: 'string'
    },
    label: {
      description: 'Checkbox label. It should be string',
      type: 'string'
    },
    checked: {
      defaultValue: { summary: false },
      description: "Checked checkbox. It can be set as true or false",
      type: 'boolean'
    },
    disabled: {
      defaultValue: { summary: false },
      description: "Disabled checkbox. If checkbox is disabled, it can't be clicked",
      type: 'boolean'
    },
    labelPosition: {
      control: { type: 'radio' },
      defaultValue: { summary: 'right' },
      description: "Checkbox label position. One of type: `left`, `right`",
      options: ['left', 'right'],
    },
    value: {
      description: "Checkbox value. When checkbox is clicked, the callback will return its value. It should be string",
      type: 'string'
    },
  },
  render: (args) => html`<signal-checkbox ?checked=${args.checked} ?disabled=${args.disabled} labelPosition=${args.labelPosition} value=${args.value}>${args.label}</signal-checkbox>`,
} as Meta

export const Default: StoryObj = {
  name: 'Default',
  args: {
    label: 'Hey, this is checkbox label',
    checked: false,
    disabled: false,
    labelPosition: 'right'
  },
}