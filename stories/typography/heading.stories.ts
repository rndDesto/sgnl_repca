import { Meta, StoryObj } from '@storybook/web-components'
import { html } from 'lit'
import '../../src/components'

const ContohSatu = (args)=>{
  return html`
  <signal-heading size=${args.size} weight=${args.weight}>${args.text}</signal-heading>
  `
}

export default {
  title: 'Typography/Heading',
  parameters: {
    design: {
      type: 'figma',
      url: 'https://www.figma.com/file/tI9cLemXH9Z2mwYQb0Pn47/Signal---Web-Kit?type=design&node-id=1597-417930&mode=design&t=4FQKB1iGtZatd8Fz-0',
    },
    docs: {
      description: {
        component: 'Component Heading digunakan untuk penamaan sebuah page',
      },
      story: { autoplay: true },
    },
  },
  argTypes: {
    text: {
      description: 'It should be string',
      type: "string",
    },
    size: {
      control: { type: 'radio' },
      defaultValue: { summary: '1' },
      description: 'Heading size. One of type: `1`, `2`, `3`, `4`,`5`,`6`',
      options: ['1', '2', '3', '4','5','6'],
    },
    weight: {
      control: { type: 'radio' },
      defaultValue: { summary: 'normal' },
      description: 'Heading weight. One of type: `bold`, `semi`, `normal`',
      options: ['bold', 'semi', 'normal'],
    }
  },
  render: (args) => ContohSatu(args),
} as Meta



export const Primary: StoryObj = {
  name: 'Default',
  args: {
    text: `Telkomsel Typography Heading`,
    size: '1',
    weight:'normal'
  },
  render: (args) => ContohSatu(args)
}
