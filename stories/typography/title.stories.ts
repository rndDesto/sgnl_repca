import { Meta, StoryObj } from '@storybook/web-components'
import { html } from 'lit'
import '../../src/components'

const ContohSatu = (args)=>{
  return html`
  <signal-title size=${args.size} weight=${args.weight}>${args.text}</signal-title>
  `
}

export default {
  title: 'Typography/Title',
  parameters: {
    design: {
      type: 'figma',
      url: 'https://www.figma.com/file/tI9cLemXH9Z2mwYQb0Pn47/Signal---Web-Kit?type=design&node-id=1597-417930&mode=design&t=4FQKB1iGtZatd8Fz-0',
    },
    docs: {
      description: {
        component: 'Component Title yang digunakan untuk sebuah judul pada artikel',
      },
      story: { autoplay: true },
    },
  },
  argTypes: {
    text: {
      description: 'It should be string',
      type: "string",
    },
    weight: {
      control: { type: 'radio' },
      defaultValue: { summary: 'normal' },
      description: 'Title weight. One of type: `bold`, `normal`',
      options: ['bold', 'normal'],
    }
  },
  render: (args) => ContohSatu(args),
} as Meta



export const Primary: StoryObj = {
  name: 'Default',
  args: {
    text: `Telkomsel Typography Title`,
    weight:'normal'
  },
  render: (args) => ContohSatu(args)
}
