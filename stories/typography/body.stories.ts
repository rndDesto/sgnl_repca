import { Meta, StoryObj } from '@storybook/web-components'
import { html } from 'lit'
import '../../src/components'

const ContohSatu = (args)=>{
  return html`
  <signal-body size=${args.size} weight=${args.weight}>${args.text}</signal-body>
  `
}

export default {
  title: 'Typography/Body',
  parameters: {
    design: {
      type: 'figma',
      url: 'https://www.figma.com/file/tI9cLemXH9Z2mwYQb0Pn47/Signal---Web-Kit?type=design&node-id=1597-417930&mode=design&t=4FQKB1iGtZatd8Fz-0',
    },
    docs: {
      description: {
        component: 'Component Body yang digunakan paragraph atau konten pada artikel',
      },
      story: { autoplay: true },
    },
  },
  argTypes: {
    text: {
      description: 'It should be string',
      type: "string",
    },
    size: {
      control: { type: 'radio' },
      defaultValue: { summary: '1' },
      description: 'Body size. One of type: `1`, `2`, `3`',
      options: ['1', '2', '3'],
    },
    weight: {
      control: { type: 'radio' },
      defaultValue: { summary: 'normal' },
      description: 'Body weight. One of type: `bold`, `normal`',
      options: ['bold', 'normal'],
    }
  },
  render: (args) => ContohSatu(args),
} as Meta



export const Primary: StoryObj = {
  name: 'Default',
  args: {
    text: `Telkomsel Typography Body`,
    size:'1',
    weight:'normal'
  },
  render: (args) => ContohSatu(args)
}
