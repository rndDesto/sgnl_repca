import { Meta, StoryObj } from '@storybook/web-components'
import { html } from 'lit'
import { withActions } from '@storybook/addon-actions/decorator'
import '../src/components'

export default {
  title: 'Selection Control/Toggle',
  parameters: {
    actions: {
      handles: ['onChange'],
    },
    design: {
      type: 'figma',
      url: 'https://www.figma.com/file/tI9cLemXH9Z2mwYQb0Pn47/Signal---Web-Kit?type=design&node-id=4407-333150&mode=design&t=4FQKB1iGtZatd8Fz-0',
    },
    docs: {
      description: {
        component: 'Element that allows users to switch between two states or modes, typically on or off. It consists of a small switch or button that can be toggled on or off by clicking or tapping on it.',
      },
      story: { autoplay: true },
    },
  },
  argTypes: {
    class: {
      description: 'The `class` attribute is often used to point to a class name in a style sheet. It can also be used by a JavaScript to access and manipulate elements with the specific class name',
      type: 'string'
    },
    label: {
      description: 'Toggle label. It should be string',
      type: 'string'
    },
    checked: {
      defaultValue: { summary: false },
      description: 'Checked toggle. If it is set as true, checked state will be shown',
      type: 'boolean'
    },
    disabled: {
      defaultValue: { summary: false },
      description: "Disabled toggle. If it is set as true, toggle can't be clicked",
      type: 'boolean'
    },
    labelPosition: {
      control: { type: 'radio' },
      defaultValue: { summary: 'right' },
      description: "Checkbox label position. One of type: `left`, `right`",
      options: ['left', 'right'],
    },
    value: {
      description: "Toggle value. When toggle is clicked, the callback will return its value. It should be string",
      type: 'string'
    },
  },
  decorators: [withActions],
  render: (args) => html`<signal-toggle ?checked=${args.checked} ?disabled=${args.disabled} labelPosition=${args.labelPosition} value=${args.value}>${args.label}</signal-toggle>`,
} as Meta

export const Default: StoryObj = {
  name: 'Default',
  args: {
    label: 'Hey, this is checkbox label',
    checked: false,
    disabled: false,
    labelPosition: 'right'
  },
}