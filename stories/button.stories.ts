import { Meta, StoryObj } from '@storybook/web-components'
import { html } from 'lit'
import { withActions } from '@storybook/addon-actions/decorator'
import '../src/components'

export default {
  title: 'Buttons/Action Button',
  parameters: {
    actions: {
      handles: ['onClick'],
    },
    design: {
      type: 'figma',
      url: 'https://www.figma.com/file/tI9cLemXH9Z2mwYQb0Pn47/Signal---Web-Kit?type=design&node-id=2111-361187&mode=design&t=4FQKB1iGtZatd8Fz-0',
    },
    docs: {
      description: {
        component: 'An action button is a graphical element on a website, app, or digital document that is designed to prompt a user to take a specific action.',
      },
      story: { autoplay: true },
    },
  },
  decorators: [withActions],
  argTypes: {
    partName: {
      description: 'Custom style name to overide existing class with pseudo selector `part`.It should be string <br/> example: type `partstyle` as input',
      type: 'string'
    },
    class: {
      description: '<del>The `class` attribute is often used to point to a class name in a style sheet. It can also be used by a JavaScript to access and manipulate elements with the specific class name</del> (DEPRECATED: use partName to inject direct style with pseudo part)',
      type: 'string'
    },
    text: {
      description: 'Button text. It should be string',
      type: "string",
    },
    variant: {
      control: { type: 'radio' },
      defaultValue: { summary: 'primary' },
      description: 'Button variant. One of type: `primary`, `secondary`, `text`',
      options: ['primary', 'secondary', 'text'],
    },
    size: {
      control: { type: 'radio' },
      defaultValue: { summary: 'xl' },
      description: 'Button size. One of type: `xl`, `lg`, `md`, `sm`',
      options: ['xl', 'lg', 'md', 'sm'],
    },
    disabled: {
      defaultValue: { summary: false },
      description: "Disabled button. If button is disabled, it can't be clicked",
      type: "boolean",
    },
    leftIcon: {
      description: "Button left icon. It should be string, can be url or local path. eg: `https://www.edigitalagency.com.au/wp-content/uploads/new-Instagram-logo-white-glyph.png` or `/assets/icon.png`",
      type: "string",
    },
    rightIcon: {
      description: "Button right icon. It should be string, can be url or local path. eg: `https://www.edigitalagency.com.au/wp-content/uploads/new-Instagram-logo-white-glyph.png` or `/assets/icon.png`",
      type: "string",
    },
    type: {
      control: { type: 'radio' },
      defaultValue: { summary: 'button' },
      description: 'Button type. One of type: `button`, `reset`, `submit`',
      options: ['button', 'reset', 'submit'],
    }
  },
  render: (args) => html`
  <style>
      signal-button::part(partstyle) {
        background: linear-gradient(90deg, red, orange, yellow, green, blue, indigo, violet);
        color: blue;
        margin: 20px;
        display: flex;
        flex-direction:column;
      }
    </style>
  <signal-button 
  partName=${args.partName} 
  type=${args.type} 
  variant=${args.variant} 
  size=${args.size} 
  ?disabled=${args.disabled} 
  leftIcon=${args.leftIcon} 
  rightIcon=${args.rightIcon}>${args.text}</signal-button>`,
} as Meta

export const Primary: StoryObj = {
  name: 'Primary',
  args: {
    type: 'button',
    text: 'BELI PULSA',
    variant: 'primary',
    size: 'xl',
    disabled: false,
    leftIcon: 'https://storybook.js.org/images/placeholders/350x150.png',
    rightIcon: 'https://storybook.js.org/images/placeholders/350x150.png'
  },
}

export const Secondary: StoryObj = {
  name: 'Secondary',
  args: {
    text: 'BELI PULSA',
    variant: 'secondary',
    size: 'xl',
    disabled: false,
    leftIcon: 'https://storybook.js.org/images/placeholders/350x150.png',
    rightIcon: 'https://storybook.js.org/images/placeholders/350x150.png'
  },
}

export const Text: StoryObj = {
  name: 'Text',
  args: {
    text: 'BELI PULSA',
    variant: 'text',
    size: 'xl',
    disabled: false,
    leftIcon: 'https://storybook.js.org/images/placeholders/350x150.png',
    rightIcon: 'https://storybook.js.org/images/placeholders/350x150.png'
  },
}