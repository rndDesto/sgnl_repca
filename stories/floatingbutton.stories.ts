import { Meta, StoryObj } from '@storybook/web-components'
import { html } from 'lit'
import { withActions } from '@storybook/addon-actions/decorator'
import '../src/components'

export default {
  title: 'Buttons/Floating Button',
  parameters: {
    actions: {
      handles: ['onClick'],
    },
    design: {
      type: 'figma',
      url: 'https://www.figma.com/file/tI9cLemXH9Z2mwYQb0Pn47/Signal---Web-Kit?type=design&node-id=326-97568&mode=design&t=4FQKB1iGtZatd8Fz-0',
    },
    docs: {
      description: {
        component: 'A floating button is a type of action button that remains visible and accessible to users as they scroll through a webpage or app. Unlike static buttons that are fixed in one position on a page, typically staying in a fixed position relative to the browser window or device screen.',
      },
      story: { autoplay: true },
    },
  },
  decorators: [withActions],
  argTypes: {
    class: {
      description: 'The `class` attribute is often used to point to a class name in a style sheet. It can also be used by a JavaScript to access and manipulate elements with the specific class name',
      type: 'string'
    },
    badge: {
      description: 'Badge floating button. It should be string',
      type: "string"
    },
    icon: {
      description: "Icon floating button. It should be string, can be url or local path. eg: `https://www.edigitalagency.com.au/wp-content/uploads/new-Instagram-logo-white-glyph.png` or `/assets/icon.png`",
      type: "string"
    },
  },
  render: (args) => html`<signal-button-floating badge=${args.badge} icon=${args.icon}></signal-button-floating>`,
} as Meta

export const Default: StoryObj = {
  name: 'Default',
  args: {
    badge: '20',
    icon: 'https://www.edigitalagency.com.au/wp-content/uploads/new-Instagram-logo-white-glyph.png'
  },
}